package assignment02.mvc.model.packagereport;

import assignment02.mvc.model.ProjectElement;
import assignment02.mvc.model.Report;
import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.classreport.fieldreport.FieldReport;
import assignment02.mvc.model.classreport.methodreport.MethodReport;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** Models a report for a java package. */
@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		property = "type")
@JsonSubTypes({
		@JsonSubTypes.Type(value = BasicPackageReport.class, name = "BasicPackageReport")
})
@JsonDeserialize(as = BasicPackageReport.class)
public interface PackageReport extends ProjectElement, Report {

	@Override
	default ProjectElementType getProjectElementType(){
		return this.isProject() ? ProjectElementType.PROJECT : ProjectElementType.PACKAGE;
	}

	/** @return true if this package report is a project report, false otherwise. */
	@JsonProperty("isProject") boolean isProject();
	/** @return a list of reports for all the subpackages that are direct children of this package. */
	@JsonProperty("childSubpackages") List<PackageReport> getChildSubpackages();
	/** @return a list of reports for all the classes that are directly defined in this package.
	 *  @apiNote subclasses of those classes are not considered to be directly defined in this package. */
	@JsonProperty("childClasses") List<ClassReport> getChildClasses();

	/**
	 * Sets the property that tells if this report is a project report to the specified value.
	 * @param isProject the specified value
	 * @return this
	 */
	PackageReport setIsProject(boolean isProject);
	/**
	 * Adds the specified package reports to the report of this package.
	 * @param packages the specified package reports
	 * @return this
	 */
	PackageReport addSubpackages(Collection<PackageReport> packages);
	/**
	 * Adds the specified class reports to the report of this package.
	 * @param classes the specified class reports
	 * @return this
	 */
	PackageReport addClasses(Collection<ClassReport> classes);

	/** Alternative to {@link #addSubpackages(Collection)}. */
	default PackageReport addSubpackages(PackageReport ...packages) {
		return this.addSubpackages(Arrays.stream(packages).collect(Collectors.toList()));
	}
	/** Alternative to {@link #addClasses(Collection)}. */
	default PackageReport addClasses(ClassReport ...classes) {
		return this.addClasses(Arrays.stream(classes).collect(Collectors.toList()));
	}

	/* ### QUERY METHODS ################################################################################################################################# */
	/** @return a list of reports for all the subpackages defined in this package. */
	default List<PackageReport> searchAllSubpackages(){
		return this.getChildSubpackages()
				   .stream()
				   .flatMap(packageReport -> Stream.concat(
					   Stream.of(packageReport),
					   packageReport.searchAllSubpackages().stream()
				   ))
				   .collect(Collectors.toList());
	}
	/** @return a list of reports for all the classes defined in this package. */
	default List<ClassReport> searchAllClasses(){
		return Stream.concat(
				this.getChildClasses()
					.stream()
					.flatMap(classReport -> Stream.concat(
							Stream.of(classReport),
							classReport.searchAllInnerClasses().stream()
					)),
				this.getChildSubpackages().stream().flatMap(packageReport -> packageReport.searchAllClasses().stream())
		).collect(Collectors.toList());
	}
	/** @return a list of reports for all the main classes defined in this package. */
	default List<ClassReport> searchMainClasses(){
		return this.searchAllClasses().stream()
				.filter(classReport -> classReport.getMethods().stream()
						.anyMatch(method -> method.getName().equals("main")))
				.collect(Collectors.toList());
	}
	/**
	 * @param className the specified class name
	 * @return an optional of the class report of the class with the specified name,
	 *         or an empty optional if the class was not found in this package.
	 */
	default Optional<ClassReport> searchClass(String className){
		return this.searchAllClasses().stream().filter(report -> report.getName().equals(className)).findFirst();
	}
	/** @return a list of reports for all the methods of the classes that are directly defined in this package. */
	default List<MethodReport> searchChildMethods(){ return PackageReport.extractMethods(this.getChildClasses()); }
	/** @return a list of reports for all the methods defined in this package. */
	default List<MethodReport> searchAllMethods(){ return PackageReport.extractMethods(this.searchAllClasses()); }
	/** @return a list of reports for all the fields of the classes that are directly defined in this package. */
	default List<FieldReport> searchChildFields(){ return PackageReport.extractFields(this.getChildClasses()); }
	/** @return a list of reports for all the fields defined in this package. */
	default List<FieldReport> searchAllFields(){ return PackageReport.extractFields(this.searchAllClasses()); }

	/**
	 * @param classes the specified classes
	 * @return a list of the methods of all the specified classes
	 */
	private static List<MethodReport> extractMethods(Collection<ClassReport> classes){
		return classes.stream().flatMap(classReport -> classReport.getMethods().stream()).collect(Collectors.toList());
	}
	/**
	 * @param classes the specified classes
	 * @return a list of the fields of all the specified classes
	 */
	private static List<FieldReport> extractFields(Collection<ClassReport> classes){
		return classes.stream().flatMap(classReport -> classReport.getFields().stream()).collect(Collectors.toList());
	}

	/* ### CASTING SUPER METHODS ######################################################################################################################### */
	@Override PackageReport setName(String name);
	@Override PackageReport setFullName(String fullName);
	@Override default PackageReport showContent() { return (PackageReport) Report.super.showContent(); }
}
