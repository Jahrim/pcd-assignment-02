package assignment02.mvc.model.classreport.methodreport;

import java.util.Objects;

/** Basic implementation for a method parameter. */
public class BasicMethodParameter implements MethodParameter {

    private String name = "NOT_SET";
    private String typeName = "NOT_SET";

    @Override
    public String getName() { return this.name; }
    @Override
    public String getTypeName() { return this.typeName; }

    @Override
    public BasicMethodParameter setName(String name) { this.name = name; return this; }
    @Override
    public BasicMethodParameter setTypeName(String typeName) { this.typeName = typeName; return this; }

    @Override
    public String toString() { return this.name + ": " + this.typeName; }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasicMethodParameter that = (BasicMethodParameter) o;
        return Objects.equals(name, that.name) && Objects.equals(typeName, that.typeName);
    }
    @Override
    public int hashCode() { return Objects.hash(name, typeName); }

}
