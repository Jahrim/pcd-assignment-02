package assignment02.mvc.controller;

import assignment02.PathManager;
import assignment02.analyzer.async.BasicVertxConcurrentProjectAnalyzer;
import assignment02.analyzer.async.VertxConcurrentProjectAnalyzer;
import assignment02.analyzer.async.verticles.VerticleContext;
import assignment02.mvc.model.ProjectElement;
import assignment02.mvc.model.ProjectElement.ProjectElementType;
import assignment02.mvc.model.packagereport.PackageReport;
import assignment02.mvc.view.MainFXView;
import assignment02.mvc.view.ImageReference;
import assignment02.mvc.view.components.ReportTreeItem;
import io.vertx.core.Vertx;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;

import javax.annotation.Nullable;
import java.io.File;
import java.net.URL;
import java.util.*;

public class MainFXController implements Initializable {

    private static final Map<ProjectElementType, Image> projectElementIcons = new HashMap<>(){{
        put(ProjectElementType.PROJECT, ImageReference.ProjectIcon_16x16.image());
        put(ProjectElementType.PACKAGE, ImageReference.PackageIcon_16x16.image());
        put(ProjectElementType.INTERFACE, ImageReference.InterfaceIcon_16x16.image());
        put(ProjectElementType.CLASS, ImageReference.ClassIcon_16x16.image());
        put(ProjectElementType.FIELD, ImageReference.FieldIcon_16x16.image());
        put(ProjectElementType.METHOD, ImageReference.MethodIcon_16x16.image());
    }};

    @FXML public TreeView<ProjectElement> projectReportView;
    @FXML public TextArea selectedElementDescription;
    @FXML public TextField selectedProjectFolderPath;
    @FXML public Text applicationStatus;

    @Nullable private MainFXView view = null;
    @Nullable private VertxConcurrentProjectAnalyzer projectAnalyzer = null;
    @Nullable private File selectedProjectFolder = null;
    @Nullable private VerticleContext<PackageReport> currentContext = null;

    /**
     * Attach the specified view to this controller.
     * @param view the specified view
     */
    public void attachView(final MainFXView view){
        this.view = view;
        this.view.getStage().setOnCloseRequest(ignored -> System.exit(0));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.projectAnalyzer = new BasicVertxConcurrentProjectAnalyzer(Vertx.vertx());
        this.selectedProjectFolder = PathManager.getManager().path().toFile();
        this.selectedProjectFolderPath.setText(this.selectedProjectFolder.getAbsolutePath());
        this.projectReportView.setCellFactory((tree) -> new TreeCell<>(){
            @Override
            protected void updateItem(ProjectElement item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty){
                    setText(null);
                    setGraphic(null);
                } else {
                    this.setText(item.getName());
                    this.setGraphic(new ImageView(projectElementIcons.get(item.getProjectElementType())));
                    this.setOnMouseClicked((mouseEvent) -> selectedElementDescription.setText(item.toString()));
                }
            }
        });
    }

    @FXML public void onSelectFolderClicked(ActionEvent actionEvent) {
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setTitle("Select a project folder to analyze...");
        File selectedProjectFolder = directoryChooser.showDialog(null);
        if (selectedProjectFolder != null){
            this.selectedProjectFolder = selectedProjectFolder;
            this.selectedProjectFolderPath.setText(selectedProjectFolder.getAbsolutePath());
        }
    }

    @FXML public void onStartClicked(ActionEvent actionEvent) {
        if (this.selectedProjectFolder != null){
            this.onStopClicked(actionEvent);
            log("Project analyzer is running...");
            TreeItem<ProjectElement> rootItem = new TreeItem<>(null);
            this.projectReportView.setShowRoot(false);
            Objects.requireNonNull(this.projectAnalyzer);
            this.currentContext = this.projectAnalyzer.analyzeProject(this.selectedProjectFolder, (projectElement) -> Platform.runLater(() -> {
                rootItem.getChildren().add(new TreeItem<>(projectElement));
                rootItem.getChildren().sort(Comparator.comparing(TreeItem::getValue));
                this.projectReportView.setRoot(rootItem);
            }));
            this.currentContext.getResult().onSuccess(projectReport -> Platform.runLater(() -> {
                this.projectReportView.setShowRoot(true);
                this.projectReportView.setRoot(new ReportTreeItem(projectReport));
                log("Project analysis completed.");
            }));
        } else {
            log("Please select a project folder before starting the analysis.");
        }
    }

    @FXML public void onStopClicked(ActionEvent actionEvent) {
        if (currentContext != null) {
            log("Project analyzer is stopping...");
            this.currentContext.interrupt().onSuccess(ignored ->
                log("Project analyzer successfully stopped. Some backed up events may still be dispatched.")
            );
            this.currentContext = null;
        } else {
            log("Please start the project analyzer before trying to stop it.");
        }
    }

    /**
     * Prints the specified status on the gui.
     * @param newStatus the specified status
     */
    private void log(final String newStatus){ this.applicationStatus.setText(newStatus); }

}
