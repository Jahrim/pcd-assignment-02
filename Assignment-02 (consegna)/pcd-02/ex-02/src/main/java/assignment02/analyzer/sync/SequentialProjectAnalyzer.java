package assignment02.analyzer.sync;

import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.packagereport.PackageReport;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Optional;

/**
 * Models a project analyzer that execute sequentially and synchronously.
 * A project analyzer allows you to search for information inside a certain java project.
 */
public interface SequentialProjectAnalyzer {

	/**
	 * @param file the file where the requested interface is defined
	 * @param name the name of the requested interface
	 * @return an optional of the report about the requested interface,
	 * 		   or an empty optional if the requested interface was not
	 * 		   found in the specified file.
	 */
	Optional<ClassReport> getInterfaceReport(File file, String name) throws FileNotFoundException;

	/**
	 * @param file the file where the requested class is defined
	 * @param name the name of the requested class
	 * @return an optional of the report about the requested class,
	 * 		   or an empty optional if the requested class was not
	 * 		   found in the specified file.
	 */
	Optional<ClassReport> getClassReport(File file, String name) throws FileNotFoundException;

	/**
	 * @param directory the directory representing the requested package
	 * @return an optional of the report about the requested package,
	 * 		   or an empty optional if the specified directory was not found.
	 */
	Optional<PackageReport> getPackageReport(File directory);

	/**
	 * @param directory the directory representing the requested project
	 * @return an optional of the report about the requested project,
	 * 		   or an empty optional if the specified directory was not found.
	 */
	Optional<PackageReport> getProjectReport(File directory);

}
