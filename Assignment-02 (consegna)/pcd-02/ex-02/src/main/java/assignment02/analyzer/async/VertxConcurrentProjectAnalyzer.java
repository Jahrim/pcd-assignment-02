package assignment02.analyzer.async;

import assignment02.analyzer.async.verticles.VerticleContext;
import assignment02.mvc.model.ProjectElement;
import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.packagereport.PackageReport;
import io.vertx.core.Future;

import java.io.File;
import java.util.function.Consumer;

/**
 * Models a project analyzer that allows you to search for information inside a
 * certain java project. Based on Vertx.
 */
public interface VertxConcurrentProjectAnalyzer {

	/**
	 * @param file the file where the requested interface is defined
	 * @param name the name of the requested interface
	 * @return a future containing the report about the requested interface.
	 */
	Future<ClassReport> getInterfaceReport(File file, String name);

	/**
	 * @param file the file where the requested class is defined
	 * @param name the name of the requested class
	 * @return a future containing the report about the requested class.
	 */
	Future<ClassReport> getClassReport(File file, String name);

	/**
	 * @param directory the directory representing the requested package
	 * @return a future containing the report about the requested package.
	 */
	Future<PackageReport> getPackageReport(File directory);

	/**
	 * @param directory the directory representing the requested project
	 * @return a future containing the report about the requested project.
	 */
	Future<PackageReport> getProjectReport(File directory);
	
	/**
	 * Async method to analyze a project, given the specified the directory
	 * representing that project, executing the specified callback each time a new
	 * project element is found.
	 * @param directory the directory representing the requested project
	 * @param onElementDiscovered a function to be executed each time the report of a
	 *                            certain project element is produced
	 * @return a verticle context containing the report about the requested project and
	 * 		   a reference to the verticle used to produce that report.
	 * @apiNote the callbacks are guaranteed to be terminated before the final project
	 *          report is produced.
	 */
	VerticleContext<PackageReport> analyzeProject(File directory, Consumer<ProjectElement> onElementDiscovered);

}
