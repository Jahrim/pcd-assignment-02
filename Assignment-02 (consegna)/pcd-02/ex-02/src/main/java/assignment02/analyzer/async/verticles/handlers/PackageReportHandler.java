package assignment02.analyzer.async.verticles.handlers;

import assignment02.PathManager;
import assignment02.analyzer.async.verticles.AbstractVerticleHandler;
import assignment02.analyzer.async.verticles.ProjectAnalyzerVerticle;
import assignment02.analyzer.async.verticles.messages.PackageMessage;
import assignment02.mvc.model.packagereport.BasicPackageReport;
import assignment02.mvc.model.packagereport.PackageReport;
import assignment02.mvc.model.packagereport.PartialPackageReport;
import io.vertx.core.CompositeFuture;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

import java.io.File;
import java.util.List;

/**
 * Models a handler for a {@link PackageMessage} responding with the requested
 * package report.
 */
public class PackageReportHandler extends AbstractVerticleHandler<ProjectAnalyzerVerticle, JsonObject> {

    public PackageReportHandler(ProjectAnalyzerVerticle ownerVerticle) { super(ownerVerticle); }

    @Override
    public void handle(Message<JsonObject> message) {
        PackageMessage data = message.body().mapTo(PackageMessage.class);
        List<File> childFiles = PathManager.getManager(data.directory.toPath()).childFilesOf();
        String packageFullName = data.parentPackageFullName == null ? "" : (data.parentPackageFullName + ".") + data.directory.getName();

        CompositeFuture getAllFileReports = this.getOwnerVerticle().getAllFileReports(childFiles);
        CompositeFuture getAllSubpackages = this.getOwnerVerticle().getAllPackageReports(childFiles, packageFullName);

        CompositeFuture.all(getAllFileReports, getAllSubpackages).onSuccess(resultFuture -> {
            List<PartialPackageReport> fileReports = ((CompositeFuture)resultFuture.resultAt(0)).list();
            List<PackageReport> packageReports = ((CompositeFuture)resultFuture.resultAt(1)).list();
            BasicPackageReport packageReport =
                    fileReports.stream()
                               .reduce(PartialPackageReport::merge)
                               .orElse(new PartialPackageReport())
                               .seal()
                               .setIsProject(data.isProject)
                               .setName(data.directory.getName())
                               .setFullName(data.isProject ? data.directory.getAbsolutePath() : packageFullName)
                               .addSubpackages(packageReports);
            this.getOwnerVerticle().discover(packageReport);
            message.reply(JsonObject.mapFrom(packageReport));
        }).onFailure(err -> { err.printStackTrace(); message.fail(404, err.toString()); });
    }

}
