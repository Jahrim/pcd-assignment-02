package assignment02.analyzer.async.verticles.messages;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.File;

/** Models a message for getting a file report through the event bus of a vertx. */
public class FileMessage {
    public final File file;

    public FileMessage(
        @JsonProperty("file") final File file
    ){
        this.file = file;
    }

    @Override
    public String toString() {
        return "FileMessage{" +
                "file=" + file +
                '}';
    }
}
