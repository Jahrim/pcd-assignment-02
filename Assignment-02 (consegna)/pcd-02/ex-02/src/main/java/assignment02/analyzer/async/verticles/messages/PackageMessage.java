package assignment02.analyzer.async.verticles.messages;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.File;

/** Models a message for getting a package or project report through the event bus of a vertx. */
public class PackageMessage {
    public final File directory;
    public final String parentPackageFullName;
    public final boolean isProject;

    public PackageMessage(
        @JsonProperty("directory") final File directory,
        @JsonProperty("parentPackageReportFullName") final String parentPackageFullName,
        @JsonProperty("isProject") final boolean isProject
    ){
        this.directory = directory;
        this.parentPackageFullName = parentPackageFullName;
        this.isProject = isProject;
    }

    @Override
    public String toString() {
        return "PackageMessage{" +
                "directory=" + directory +
                ", parentPackageFullName='" + parentPackageFullName + '\'' +
                '}';
    }
}
