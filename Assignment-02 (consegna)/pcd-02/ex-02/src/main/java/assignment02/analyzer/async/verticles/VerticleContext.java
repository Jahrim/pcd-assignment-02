package assignment02.analyzer.async.verticles;

import io.vertx.core.Future;
import io.vertx.core.Verticle;

/**
 * Models a result within the context of a certain verticle.
 * @param <T> the type of the result
 */
public class VerticleContext<T> {
    private final Verticle verticle;
    private final Future<String> deploymentID;
    private final Future<T> result;

    public VerticleContext(final Verticle verticle, final Future<String> deploymentID, final Future<T> result){
        this.verticle = verticle;
        this.deploymentID = deploymentID;
        this.result = result;
    }

    /** @return the result of this context. */
    public Future<T> getResult() { return this.result; }
    /**
     * Interrupts the execution of this verticle.
     * @return an empty future completed when the verticle has been successfully undeployed.
     */
    public Future<Void> interrupt(){
        if (this.deploymentID.succeeded()){
            return this.verticle.getVertx().deploymentIDs().contains(this.deploymentID.result())
                    ? this.verticle.getVertx().undeploy(this.deploymentID.result())
                    : Future.succeededFuture();
        } else {
            return this.deploymentID.transform(deploymentID -> this.verticle.getVertx().undeploy(deploymentID.result()));
        }
    }

}