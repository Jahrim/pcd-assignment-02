package assignment02.analyzer.async.verticles.handlers;

import assignment02.analyzer.async.verticles.AbstractVerticleHandler;
import assignment02.analyzer.async.verticles.ProjectAnalyzerVerticle;
import assignment02.analyzer.async.verticles.messages.ClassMessage;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

import java.util.NoSuchElementException;

/**
 * Models an handler for a {@link ClassMessage} responding with the requested
 * class or interface report.
 */
public class ClassReportHandler extends AbstractVerticleHandler<ProjectAnalyzerVerticle, JsonObject> {

    private final boolean requireInterface;

    public ClassReportHandler(final ProjectAnalyzerVerticle ownerVerticle, final boolean requireInterface) {
        super(ownerVerticle);
        this.requireInterface = requireInterface;
    }

    @Override
    public void handle(Message<JsonObject> message) {
        ClassMessage data = message.body().mapTo(ClassMessage.class);
        this.getOwnerVerticle().getFileReport(data.file).onSuccess(fileReport -> {
            try {
                message.reply(
                    JsonObject.mapFrom(
                        fileReport.searchClass(data.className)
                                  .filter(classReport -> classReport.isInterface() == requireInterface)
                                  .orElseThrow()
                    )
                );
            } catch (NoSuchElementException e) { e.printStackTrace(); message.fail(404, e.getMessage()); }
        }).onFailure(err -> { err.printStackTrace(); message.fail(404, err.getMessage()); } );
    }
}
