package assignment02.analyzer.async;

import assignment02.analyzer.async.verticles.ProjectAnalyzerVerticle;
import assignment02.analyzer.async.verticles.VerticleContext;
import assignment02.mvc.model.ProjectElement;
import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.packagereport.PackageReport;
import io.vertx.core.Future;
import io.vertx.core.Vertx;

import java.io.File;
import java.util.function.Consumer;
import java.util.function.Function;

/** Basic implementation of a concurrent project analyzer. */
public class BasicVertxConcurrentProjectAnalyzer implements VertxConcurrentProjectAnalyzer {

    private final Vertx vertx;

    public BasicVertxConcurrentProjectAnalyzer(final Vertx vertx){ this.vertx = vertx; }

    @Override
    public Future<ClassReport> getInterfaceReport(final File file, final String name) {
        return this.useDefaultVerticleOnce(verticle -> verticle.getInterfaceReport(file, name)).getResult();
    }

    @Override
    public Future<ClassReport> getClassReport(final File file, final String name) {
        return this.useDefaultVerticleOnce(verticle -> verticle.getClassReport(file, name)).getResult();
    }

    @Override
    public Future<PackageReport> getPackageReport(final File directory) {
        return this.useDefaultVerticleOnce(verticle -> verticle.getPackageReport(directory, null, false)).getResult();
    }

    @Override
    public Future<PackageReport> getProjectReport(final File directory) {
        return this.useDefaultVerticleOnce(verticle -> verticle.getPackageReport(directory, null, true)).getResult();
    }

    @Override
    public VerticleContext<PackageReport> analyzeProject(final File directory, final Consumer<ProjectElement> onElementDiscovered) {
        return this.useVerticleOnce(new ProjectAnalyzerVerticle(onElementDiscovered), verticle -> verticle.getPackageReport(directory, null, true));
    }

    /**
     * @param verticle the specified verticle
     * @param task the specified task
     * @param <T> the return type of the specified task
     * @return a verticle context containing the result of the specified task executed by the specified
     *         verticle, undeploying the verticle once the task is completed.
     */
    private <T> VerticleContext<T> useVerticleOnce(final ProjectAnalyzerVerticle verticle, final Function<ProjectAnalyzerVerticle, Future<T>> task){
        Future<String> deploymentID = vertx.deployVerticle(verticle);
        return new VerticleContext<>(
            verticle,
            deploymentID,
            deploymentID.transform(deploymentCompletedFuture ->
                task.apply(verticle).onComplete(ignored -> vertx.undeploy(deploymentCompletedFuture.result()))
            )
        );
    }
    /** As {@link #useVerticleOnce(ProjectAnalyzerVerticle, Function)} but it uses the default project analyzer verticle. */
    private <T> VerticleContext<T> useDefaultVerticleOnce(final Function<ProjectAnalyzerVerticle, Future<T>> task){
        return this.useVerticleOnce(new ProjectAnalyzerVerticle(), task);
    }
}
