package assignment02.junit.mvc.model.packagereport.filereport;

import assignment02.mvc.model.classreport.BasicClassReport;
import assignment02.mvc.model.classreport.fieldreport.BasicFieldReport;
import assignment02.mvc.model.classreport.methodreport.BasicMethodParameter;
import assignment02.mvc.model.classreport.methodreport.BasicMethodReport;
import assignment02.mvc.model.packagereport.filereport.FileReport;
import com.github.javaparser.ast.AccessSpecifier;

/** Utility to get the file report expected from the file ExampleClass.java. */
public final class StaticExampleClassFileReport {

    /** @return the file report of ExampleClass.java. */
    public static FileReport get(){
        String packageFullName = "assignment02.parse.examplepackage";
        return new FileReport()
                .setName("examplepackage")
                .setFullName(packageFullName)
                .addClasses(
                        new BasicClassReport()
                                .setName("ExampleClass")
                                .setFullName(packageFullName + ".ExampleClass")
                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                .setIsInterface(false)
                                .addFields(
                                        new BasicFieldReport()
                                                .setName("privateField")
                                                .setFullName(packageFullName + ".ExampleClass.privateField")
                                                .setAccessSpecifier(AccessSpecifier.PRIVATE)
                                                .setTypeName("int"),
                                        new BasicFieldReport()
                                                .setName("protectedFinalField")
                                                .setFullName(packageFullName + ".ExampleClass.protectedFinalField")
                                                .setAccessSpecifier(AccessSpecifier.PROTECTED)
                                                .setTypeName("String"),
                                        new BasicFieldReport()
                                                .setName("publicStaticFinalField")
                                                .setFullName(packageFullName + ".ExampleClass.publicStaticFinalField")
                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                .setTypeName("Object"),
                                        new BasicFieldReport()
                                                .setName("packagePrivateField")
                                                .setFullName(packageFullName + ".ExampleClass.packagePrivateField")
                                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                                .setTypeName("Double")
                                )
                                .addMethods(
                                        new BasicMethodReport()
                                                .setName("privateVoidMethod")
                                                .setFullName(packageFullName + ".ExampleClass.privateVoidMethod")
                                                .setAccessSpecifier(AccessSpecifier.PRIVATE)
                                                .setReturnTypeName("void")
                                                .setCodeBeginLine(12).setCodeEndLine(12),
                                        new BasicMethodReport()
                                                .setName("protectedFinalStringMethod")
                                                .setFullName(packageFullName + ".ExampleClass.protectedFinalStringMethod")
                                                .setAccessSpecifier(AccessSpecifier.PROTECTED)
                                                .setReturnTypeName("String")
                                                .setCodeBeginLine(13).setCodeEndLine(13),
                                        new BasicMethodReport()
                                                .setName("publicStaticObjectMethod")
                                                .setFullName(packageFullName + ".ExampleClass.publicStaticObjectMethod")
                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                .setReturnTypeName("Object")
                                                .setCodeBeginLine(14).setCodeEndLine(14),
                                        new BasicMethodReport()
                                                .setName("packagePrivateDoubleMethod")
                                                .setFullName(packageFullName + ".ExampleClass.packagePrivateDoubleMethod")
                                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                                .setReturnTypeName("Double")
                                                .setCodeBeginLine(15).setCodeEndLine(15),
                                        new BasicMethodReport()
                                                .setName("methodWithParameters")
                                                .setFullName(packageFullName + ".ExampleClass.methodWithParameters")
                                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                                .addParameters(
                                                        new BasicMethodParameter().setName("a").setTypeName("int"),
                                                        new BasicMethodParameter().setName("b").setTypeName("String")
                                                )
                                                .setReturnTypeName("void")
                                                .setCodeBeginLine(16).setCodeEndLine(16)
                                )
                                .addInnerClasses(
                                        new BasicClassReport()
                                                .setName("PublicStaticSubclass")
                                                .setFullName(packageFullName + ".ExampleClass.PublicStaticSubclass")
                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                .setIsInterface(false)
                                                .addFields(
                                                        new BasicFieldReport()
                                                                .setName("fieldA1")
                                                                .setFullName(packageFullName + ".ExampleClass.PublicStaticSubclass.fieldA1")
                                                                .setAccessSpecifier(AccessSpecifier.PRIVATE)
                                                                .setTypeName("int")
                                                )
                                                .addMethods(
                                                        new BasicMethodReport()
                                                                .setName("methodA1")
                                                                .setFullName(packageFullName + ".ExampleClass.PublicStaticSubclass.methodA1")
                                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                                .setReturnTypeName("int")
                                                                .setCodeBeginLine(20).setCodeEndLine(22)
                                                ),
                                        new BasicClassReport()
                                                .setName("PublicSubclass")
                                                .setFullName(packageFullName + ".ExampleClass.PublicSubclass")
                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                .setIsInterface(false)
                                                .addFields(
                                                        new BasicFieldReport()
                                                                .setName("fieldB1")
                                                                .setFullName(packageFullName + ".ExampleClass.PublicSubclass.fieldB1")
                                                                .setAccessSpecifier(AccessSpecifier.PRIVATE)
                                                                .setTypeName("String")
                                                )
                                                .addMethods(
                                                        new BasicMethodReport()
                                                                .setName("methodB1")
                                                                .setFullName(packageFullName + ".ExampleClass.PublicSubclass.methodB1")
                                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                                .setReturnTypeName("String")
                                                                .setCodeBeginLine(26).setCodeEndLine(26)
                                                ),
                                        new BasicClassReport()
                                                .setName("PackagePrivateSubclassWithExtension")
                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension")
                                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                                .setIsInterface(false)
                                                .addFields(
                                                        new BasicFieldReport()
                                                                .setName("fieldC1")
                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.fieldC1")
                                                                .setAccessSpecifier(AccessSpecifier.PRIVATE)
                                                                .setTypeName("Object")
                                                )
                                                .addMethods(
                                                        new BasicMethodReport()
                                                                .setName("methodC1")
                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.methodC1")
                                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                                .setReturnTypeName("Object")
                                                                .setCodeBeginLine(30).setCodeEndLine(30),
                                                        new BasicMethodReport()
                                                                .setName("methodA1")
                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.methodA1")
                                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                                .setReturnTypeName("int")
                                                                .setCodeBeginLine(32).setCodeEndLine(35)
                                                )
                                                .addInnerClasses(
                                                        new BasicClassReport()
                                                                .setName("SubclassA")
                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassA")
                                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                                .setIsInterface(false)
                                                                .addInnerClasses(
                                                                        new BasicClassReport()
                                                                                .setName("SubclassA1")
                                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassA.SubclassA1")
                                                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                                                .setIsInterface(false)
                                                                                .addFields(
                                                                                        new BasicFieldReport()
                                                                                                .setName("fieldA1")
                                                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassA.SubclassA1.fieldA1")
                                                                                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                                                                                .setTypeName("int")
                                                                                ),
                                                                        new BasicClassReport()
                                                                                .setName("SubclassA2")
                                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassA.SubclassA2")
                                                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                                                .setIsInterface(false)
                                                                                .addMethods(
                                                                                        new BasicMethodReport()
                                                                                                .setName("methodA2")
                                                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassA.SubclassA2.methodA2")
                                                                                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                                                                                .setReturnTypeName("void")
                                                                                                .setCodeBeginLine(39).setCodeEndLine(39)
                                                                                ),
                                                                        new BasicClassReport()
                                                                                .setName("SubclassA3")
                                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassA.SubclassA3")
                                                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                                                .setIsInterface(false)
                                                                                .addInnerClasses(
                                                                                        new BasicClassReport()
                                                                                                .setName("SubclassA3A")
                                                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassA.SubclassA3.SubclassA3A")
                                                                                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                                                                                .setIsInterface(false)
                                                                                )
                                                                ),
                                                        new BasicClassReport()
                                                                .setName("SubclassB")
                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassB")
                                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                                .setIsInterface(false)
                                                                .addInnerClasses(
                                                                        new BasicClassReport()
                                                                                .setName("SubclassB1")
                                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassB.SubclassB1")
                                                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                                                .setIsInterface(false)
                                                                                .addFields(
                                                                                        new BasicFieldReport()
                                                                                                .setName("fieldB1")
                                                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassB.SubclassB1.fieldB1")
                                                                                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                                                                                .setTypeName("String")
                                                                                ),
                                                                        new BasicClassReport()
                                                                                .setName("SubclassB2")
                                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassB.SubclassB2")
                                                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                                                .setIsInterface(false)
                                                                                .addMethods(
                                                                                        new BasicMethodReport()
                                                                                                .setName("methodB2")
                                                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassB.SubclassB2.methodB2")
                                                                                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                                                                                .setReturnTypeName("String")
                                                                                                .setCodeBeginLine(44).setCodeEndLine(44)
                                                                                ),
                                                                        new BasicClassReport()
                                                                                .setName("SubclassB3")
                                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassB.SubclassB3")
                                                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                                                .setIsInterface(false)
                                                                                .addInnerClasses(
                                                                                        new BasicClassReport()
                                                                                                .setName("SubclassB3A")
                                                                                                .setFullName(packageFullName + ".ExampleClass.PackagePrivateSubclassWithExtension.SubclassB.SubclassB3.SubclassB3A")
                                                                                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                                                                                .setIsInterface(false)
                                                                                )
                                                                )
                                                )
                                ),
                        new BasicClassReport()
                                .setName("OuterClass")
                                .setFullName(packageFullName + ".OuterClass")
                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                .setIsInterface(false)
                                .addFields(
                                        new BasicFieldReport()
                                                .setName("field")
                                                .setFullName(packageFullName + ".OuterClass.field")
                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                .setTypeName("ExampleClass")
                                ),
                        new BasicClassReport()
                                .setName("ExampleInterface")
                                .setFullName(packageFullName + ".ExampleInterface")
                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                .setIsInterface(true)
                                .addFields(
                                        new BasicFieldReport()
                                                .setName("field1")
                                                .setFullName(packageFullName + ".ExampleInterface.field1")
                                                .setAccessSpecifier(AccessSpecifier.PUBLIC)
                                                .setTypeName("int")
                                )
                                .addMethods(
                                        new BasicMethodReport()
                                                .setName("interfaceMethod")
                                                .setFullName(packageFullName + ".ExampleInterface.interfaceMethod")
                                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                                .setReturnTypeName("ExampleClass")
                                                .setCodeBeginLine(58).setCodeEndLine(58),
                                        new BasicMethodReport()
                                                .setName("defaultInterfaceMethod")
                                                .setFullName(packageFullName + ".ExampleInterface.defaultInterfaceMethod")
                                                .setAccessSpecifier(AccessSpecifier.PACKAGE_PRIVATE)
                                                .setReturnTypeName("ExampleClass")
                                                .setCodeBeginLine(59).setCodeEndLine(64)
                                )
                );
    }

}
