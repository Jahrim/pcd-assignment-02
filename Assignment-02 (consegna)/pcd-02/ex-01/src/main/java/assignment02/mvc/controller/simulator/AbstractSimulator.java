package assignment02.mvc.controller.simulator;

import assignment02.concurrency.monitor.Condition;
import assignment02.mvc.model.Body;
import assignment02.mvc.model.Boundary;
import assignment02.util.math.P2d;
import assignment02.util.math.V2d;
import assignment02.mvc.view.SimulationView;

import java.util.ArrayList;
import java.util.Random;

/**
 * An abstract Simulator.
 * @see Simulator
 */
public abstract class AbstractSimulator implements Simulator {

    /** View of this simulation. */
    protected SimulationView viewer = null;

    /** Bodies in the field. */
    protected ArrayList<Body> bodies;
    /** Boundary of the field. */
    protected Boundary bounds;

    /** Virtual time, indicating how much time has passed since the start of this simulation. */
    protected double virtualTime;
    /** Virtual time step, indicating how long it takes to execute an iteration of this simulation. */
    protected double dt;

    /** True if the simulation should be stopped, false otherwise. */
    protected Condition isSimulationStopped;

    @Override
    public void loop(final long maxIteration){
        /* Simulation setup */
        this.virtualTime = 0;
        this.dt = 0.001;
        onSimulationSetup();

        /* Wait for the start button to be pressed if there's a view attached to this simulator. */
        this.isSimulationStopped = new Condition(this.hasView());
        try {
            if (this.isSimulationStopped.isVerified()) { this.isSimulationStopped.awaitUpdate(); }
        } catch (InterruptedException e) { e.printStackTrace(); }

        /* Simulation loop */
        for (int i = 0; i < maxIteration && !this.isSimulationStopped.isVerified(); i++){
            onSimulationStep();
            this.virtualTime += this.dt;
            if (this.hasView()) { this.viewer.display(this.bodies, this.virtualTime, i, this.bounds); }
        }
        onSimulationEnd();
    }

    /**
     * Notifies this simulator to start the simulation.
     * @return this
     */
    public AbstractSimulator startSimulation(){ this.isSimulationStopped.update(false); return this; }
    /**
     * Notifies this simulator to stop the simulation.
     * @return this
     */
    public AbstractSimulator stopSimulation(){ this.isSimulationStopped.update(true); return this; }

    /**
     * Attach the specified view to this simulation.
     * @param viewer the specified view
     * @return this
     */
    public AbstractSimulator attachView(final SimulationView viewer){ this.viewer = viewer; return this; }
    /** @return true if this simulation has a view attached to it, false otherwise. */
    public boolean hasView() { return this.viewer != null; }

    /** A handler executed before the simulation is started. */
    protected abstract void onSimulationSetup();
    /** A handler executed during each iteration of the simulation loop. */
    protected abstract void onSimulationStep();
    /** A handler executed after the simulation has ended. */
    protected abstract void onSimulationEnd();


    /* ###################################################################################################################### */
    /* AVAILABLE TESTS */
    /* ###################################################################################################################### */
    /** Test with two bodies, one with double the mass of the other. */
    public AbstractSimulator testBodySet1_two_bodies() {
        bounds = new Boundary(-4.0, -4.0, 4.0, 4.0);
        bodies = new ArrayList<>();
        bodies.add(new Body(0, new P2d(-0.1, 0), new V2d(0,0), new V2d(0,0), 1));
        bodies.add(new Body(1, new P2d(0.1, 0), new V2d(0,0), new V2d(0,0), 2));
        return this;
    }

    /** Test with three bodies, one with ten times the mass of the others. */
    public AbstractSimulator testBodySet2_three_bodies() {
        bounds = new Boundary(-1.0, -1.0, 1.0, 1.0);
        bodies = new ArrayList<>();
        bodies.add(new Body(0, new P2d(0, 0), new V2d(0,0), new V2d(0,0), 10));
        bodies.add(new Body(1, new P2d(0.2, 0), new V2d(0,0), new V2d(0,0), 1));
        bodies.add(new Body(2, new P2d(-0.2, 0), new V2d(0,0), new V2d(0,0), 1));
        return this;
    }

    /** Test with some bodies, all with the same mass. */
    public AbstractSimulator testBodySet3_some_bodies() {
        bounds = new Boundary(-4.0, -4.0, 4.0, 4.0);
        int nBodies = 100;
        Random rand = new Random(System.currentTimeMillis());
        bodies = new ArrayList<>();
        for (int i = 0; i < nBodies; i++) {
            double x = bounds.getX0()*0.25 + rand.nextDouble() * (bounds.getX1() - bounds.getX0()) * 0.25;
            double y = bounds.getY0()*0.25 + rand.nextDouble() * (bounds.getY1() - bounds.getY0()) * 0.25;
            Body b = new Body(i, new P2d(x, y), new V2d(0, 0), new V2d(0,0), 10);
            bodies.add(b);
        }
        return this;
    }

    /** Test with a lot of bodies, all with the same mass. */
    public AbstractSimulator testBodySet4_many_bodies() {
        bounds = new Boundary(-6.0, -6.0, 6.0, 6.0);
        int nBodies = 1000;
        Random rand = new Random(System.currentTimeMillis());
        bodies = new ArrayList<>();
        for (int i = 0; i < nBodies; i++) {
            double x = bounds.getX0()*0.25 + rand.nextDouble() * (bounds.getX1() - bounds.getX0()) * 0.25;
            double y = bounds.getY0()*0.25 + rand.nextDouble() * (bounds.getY1() - bounds.getY0()) * 0.25;
            Body b = new Body(i, new P2d(x, y), new V2d(0, 0), new V2d(0,0), 10);
            bodies.add(b);
        }
        return this;
    }

    /**
     * Test with the specified amount of bodies, all with the same mass.
     * @param nBodies the specified amount of bodies
     * @return this
     */
    public AbstractSimulator testCustomBodySet(int nBodies) {
        bounds = new Boundary(-10.0, -10.0, 10.0, 10.0);
        Random rand = new Random(System.currentTimeMillis());
        bodies = new ArrayList<>();
        for (int i = 0; i < nBodies; i++) {
            double x = bounds.getX0()*0.25 + rand.nextDouble() * (bounds.getX1() - bounds.getX0()) * 0.25;
            double y = bounds.getY0()*0.25 + rand.nextDouble() * (bounds.getY1() - bounds.getY0()) * 0.25;
            Body b = new Body(i, new P2d(x, y), new V2d(0, 0), new V2d(0,0), 10);
            bodies.add(b);
        }
        return this;
    }

}
