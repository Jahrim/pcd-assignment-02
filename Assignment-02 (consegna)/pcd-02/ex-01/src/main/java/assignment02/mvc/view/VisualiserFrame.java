package assignment02.mvc.view;

import assignment02.mvc.controller.Controller;
import assignment02.mvc.model.Body;
import assignment02.mvc.model.Boundary;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

public class VisualiserFrame extends JFrame {

    private Controller controller;

    private final VisualiserPanel panel;
    private final JButton startButton, stopButton;

    public VisualiserFrame(int w, int h){
        setTitle("Bodies Simulation");
        setSize(w,h);
        setResizable(false);

        JPanel controlPanel = new JPanel();
        this.startButton = new JButton("start");
        this.stopButton = new JButton("stop");
        controlPanel.add(this.startButton);
        controlPanel.add(this.stopButton);

        this.panel = new VisualiserPanel(w,h);

        JPanel mainPanel = new JPanel();
        LayoutManager layout = new BorderLayout();
        mainPanel.setLayout(layout);
        mainPanel.add(BorderLayout.SOUTH, controlPanel);
        mainPanel.add(BorderLayout.CENTER, this.panel);
        setContentPane(mainPanel);

        startButton.setEnabled(true);
        startButton.addActionListener(ev -> {
            startButton.setEnabled(false);
            stopButton.setEnabled(true);
            controller.notifyStartSimulation();
        });

        stopButton.setEnabled(false);
        stopButton.addActionListener(ev -> {
            startButton.setEnabled(false);
            stopButton.setEnabled(false);
            controller.notifyStopSimulation();
        });

        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent ev){
                System.out.println("Window closing..."); System.exit(0);
            }
            public void windowClosed(WindowEvent ev){
                System.out.println("Window closed..."); System.exit(0);
            }
        });
        this.setVisible(true);
    }

    public void display(ArrayList<Body> bodies, double vt, long iter, Boundary bounds){
        try {
            SwingUtilities.invokeLater(() -> {
                panel.display(bodies, vt, iter, bounds);
                repaint();
            });
        } catch (Exception ignored) {}
    }

    public void updateScale(double k) {
        panel.updateScale(k);
    }
    public void attachController(Controller controller){
        this.controller = controller;
    }
}
