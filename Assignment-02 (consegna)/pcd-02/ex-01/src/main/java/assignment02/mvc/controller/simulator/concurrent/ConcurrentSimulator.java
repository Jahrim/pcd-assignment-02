package assignment02.mvc.controller.simulator.concurrent;

import assignment02.mvc.controller.simulator.AbstractSimulator;
import assignment02.concurrency.masterworker.master.Master;
import assignment02.concurrency.masterworker.taskpool.Task;
import assignment02.mvc.model.Body;

import java.util.ArrayList;
import java.util.List;

/**
 * Models a simulator whose execution is concurrent.
 */
public class ConcurrentSimulator extends AbstractSimulator {

	/** The master used to concurrently execute the simulation. */
	private final Master master;
	/** A list of subtasks used to update the velocity of the bodies in the simulation. */
	private final List<Task> updateVelocityMacroTask = new ArrayList<>();
	/** A list of subtasks used to update the position of the bodies in the simulation. */
	private final List<Task> updatePositionMacroTask = new ArrayList<>();
	/** A list of subtasks used to check the collisions of the bodies with the boundary in the simulation. */
	private final List<Task> checkCollisionWithBoundaryMacroTask = new ArrayList<>();

	public ConcurrentSimulator(final Master master) { this.master = master; }

	@Override
	protected void onSimulationSetup() {
		for (Body b: bodies) {
			this.updateVelocityMacroTask.add(SimulatorTaskFactory.createUpdateVelocityTask(b, bodies, dt));
			this.updatePositionMacroTask.add(SimulatorTaskFactory.createUpdatePositionTask(b, dt));
			this.checkCollisionWithBoundaryMacroTask.add(SimulatorTaskFactory.createCheckCollisionWithBoundaryTask(b, bounds));
		}
	}

	@Override
	protected void onSimulationStep() {
		try {
			this.master.syncExecute(this.updateVelocityMacroTask)
					   .syncExecute(this.updatePositionMacroTask)
					   .syncExecute(this.checkCollisionWithBoundaryMacroTask);
		} catch (InterruptedException e) { e.printStackTrace(); }
	}

	@Override
	protected void onSimulationEnd() {
		this.master.killWorkers();
	}

}
