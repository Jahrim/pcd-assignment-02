package assignment02.mvc.controller.simulator.concurrent;

import assignment02.mvc.model.Body;
import assignment02.mvc.model.Boundary;
import assignment02.concurrency.masterworker.taskpool.Task;
import assignment02.util.math.V2d;

import java.util.List;

/**
 * Model a static factory for simulator tasks.
 */
public class SimulatorTaskFactory {

    /**
     * @param b the specified body
     * @param bodies the bodies in the simulation
     * @param dt the elapsed time
     * @return a task which updates the velocity of the specified body, given the bodies in the simulation and
     *         elapsed time.
     */
    public static Task createUpdateVelocityTask(final Body b, final List<Body> bodies, final double dt){
        return () -> {
            V2d totalForce = b.computeTotalForceOnSelf(bodies);
            b.updateAcceleration(totalForce);
            b.updateVelocity(dt);
        };
    }

    /**
     * @param b the specified body
     * @param dt the elapsed time
     * @return a task which updates the position of the specified body, given the elapsed time.
     */
    public static Task createUpdatePositionTask(final Body b, final double dt){
        return () -> b.updatePosition(dt);
    }

    /**
     * @param b the specified body
     * @param bounds the boundary in the simulation
     * @return a task which updates the position and velocity of the specified body if it collides with
     *         the given boundary.
     */
    public static Task createCheckCollisionWithBoundaryTask(final Body b, final Boundary bounds){
        return () -> b.checkAndSolveBoundaryCollision(bounds);
    }

}
