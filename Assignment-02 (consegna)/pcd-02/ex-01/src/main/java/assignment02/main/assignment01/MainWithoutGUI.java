package assignment02.main.assignment01;

import assignment02.mvc.controller.simulator.concurrent.ConcurrentSimulator;
import assignment02.mvc.controller.Controller;
import assignment02.concurrency.masterworker.master.SinglePoolMaster;

import java.util.*;

/**
 * Executes a concurrent simulation without view for all the possible combinations
 * of the specified arguments.
 */
public class MainWithoutGUI {

    private static final int MAX_WORKERS = Runtime.getRuntime().availableProcessors() + 1;

    /** The set of #bodies arguments to test. */
    private static Set<Integer> nBodiesArgs = Set.of(100, 1000, 5000);
    /** The set of #iterations arguments to test. */
    private static Set<Integer> nIterationsArgs = Set.of(1000, 10000, 50000);
    /** The set of #workers arguments to test. */
    private static Set<Integer> nWorkersArgs = Set.of(1,2,3,4, MAX_WORKERS);
    /** A map from a specified tuple of arguments to the time of execution of the correspondent simulation. */
    private static Map<SimulationArgs, Long> timeMap = new HashMap<>();

    public static void main(String[] args) {
        for (Integer nBodies: nBodiesArgs) {
            for (Integer nIterations: nIterationsArgs) {
                for (Integer nWorkers: nWorkersArgs) {
                    SimulationArgs simArgs = new SimulationArgs(nBodies, nIterations, nWorkers);
                    System.out.println("Simulating " + simArgs + "...");
                    long time = MainWithoutGUI.simulateConcurrent(simArgs);
                    timeMap.put(simArgs, time);
                }
            }
        }
        timeMap.forEach( (simArgs, time) -> {
            System.out.println(simArgs.toString() + " => " + String.format("%.2f", time/1000f) + "s");
        });
        System.exit(0);
    }

    /**
     * Executes a concurrent simulation with the specified arguments.
     * @param simArgs the arguments for the concurrent simulation
     * @return the time of execution for the specified simulation.
     */
    public static long simulateConcurrent(SimulationArgs simArgs){
        return new Controller(new ConcurrentSimulator(new SinglePoolMaster(simArgs.nWorkers))).runSimulation(simArgs.nBodies, simArgs.nIterations);
    }
}
