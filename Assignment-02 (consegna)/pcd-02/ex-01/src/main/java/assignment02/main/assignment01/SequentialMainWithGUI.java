package assignment02.main.assignment01;

import assignment02.mvc.controller.Controller;
import assignment02.mvc.controller.simulator.sequential.SequentialSimulator;
import assignment02.mvc.view.SimulationView;

/**
 * Sequential simulation with view.
 * This main runs the sequential simulation from which this project started.
 */
public class SequentialMainWithGUI {

    private static final int nBodies = 1000;
    private static final int nIterations = 5000;

    public static void main(String[] args) {
        Controller controller = new Controller(new SequentialSimulator());
        SimulationView viewer = new SimulationView(620,620);
        long time = controller.attachView(viewer).runSimulation(nBodies, nIterations);

        final SimulationArgs simArgs = new SimulationArgs(nBodies, nIterations, 0);
        System.out.println(simArgs + " => " + String.format("%.2f", time/1000f) + "s");
        System.exit(0);
    }
}
