package assignment02.main.assignment02;

import java.util.Objects;
import java.util.concurrent.ExecutorService;

/**
 * Models a tuple of arguments for a certain simulation.
 */
public class SimulationArgs {
    public final int nBodies;
    public final int nIterations;
    public final String executorName;
    public final ExecutorService executor;

    public SimulationArgs(int nBodiesArg, int nIterationsArg, String executorName, ExecutorService executor) {
        this.nBodies = nBodiesArg;
        this.nIterations = nIterationsArg;
        this.executorName = executorName;
        this.executor = executor;
    }

    @Override
    public String toString() {
        return "SimulationArgs(" + nBodies + ", " + nIterations + ", '" + executorName + "')";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimulationArgs that = (SimulationArgs) o;
        return nBodies == that.nBodies && nIterations == that.nIterations && Objects.equals(executorName, that.executorName) && Objects.equals(executor, that.executor);
    }
    @Override
    public int hashCode() { return Objects.hash(nBodies, nIterations, executorName, executor); }
}