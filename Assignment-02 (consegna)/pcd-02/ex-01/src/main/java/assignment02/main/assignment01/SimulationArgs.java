package assignment02.main.assignment01;

import java.util.Objects;

/**
 * Models a tuple of arguments for a certain simulation.
 */
public class SimulationArgs {
    public final int nBodies;
    public final int nIterations;
    public final int nWorkers;

    public SimulationArgs(int nBodiesArg, int nIterationsArg, int nWorkersArg) {
        this.nBodies = nBodiesArg;
        this.nIterations = nIterationsArg;
        this.nWorkers = nWorkersArg;
    }

    @Override
    public String toString() {
        return "SimulationArgs("+ nBodies + ", " + nIterations + ", " + nWorkers + ')';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimulationArgs that = (SimulationArgs) o;
        return nBodies == that.nBodies && nIterations == that.nIterations && nWorkers == that.nWorkers;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nBodies, nIterations, nWorkers);
    }
}