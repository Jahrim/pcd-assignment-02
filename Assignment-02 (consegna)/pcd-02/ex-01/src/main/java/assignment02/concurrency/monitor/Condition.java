package assignment02.concurrency.monitor;

/**
 * [Monitor] Models a thread-safe condition.
 */
public class Condition {

    /** True if this condition is verified, false otherwise. */
    private boolean isVerified;

    public Condition(boolean isVerified){
        this.isVerified = isVerified;
    }

    /**
     * Waits for this condition to be updated.
     * @return this
     * @throws InterruptedException if the wait for this thread is interrupted.
     */
    public synchronized Condition awaitUpdate() throws InterruptedException {
        this.wait();
        return this;
    }

    /**
     * Updates the validity of this condition.
     * @param isVerified the new validity of this condition.
     * @return this
     */
    public synchronized Condition update(boolean isVerified){
        this.isVerified = isVerified;
        this.notifyAll();
        return this;
    }

    /**
     * @return true if this condition is verified, false otherwise.
     */
    public synchronized boolean isVerified(){
        return this.isVerified;
    }
}
