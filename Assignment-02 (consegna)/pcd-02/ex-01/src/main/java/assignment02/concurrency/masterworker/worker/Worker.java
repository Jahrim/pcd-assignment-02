package assignment02.concurrency.masterworker.worker;

import assignment02.concurrency.masterworker.taskpool.Task;
import assignment02.concurrency.masterworker.taskpool.TaskPool;
import assignment02.util.exception.UsedWhenClosedException;

import java.util.List;

/**
 * Models a worker whose job is to deplete an assigned task pool
 * until it is closed by the master.
 */
public class Worker extends Thread {

    /** The task pool which this worker extracts the tasks to be executed from. */
    private final TaskPool taskPool;
    /** The number of tasks this worker process at a time from the task pool. */
    private final int taskBatchSize;

    public Worker(final TaskPool taskPool) { this(taskPool, 1); }
    public Worker(final TaskPool taskPool, final int taskBatchSize){ this("Worker", taskPool, taskBatchSize); }
    public Worker(final String name, final TaskPool taskPool, final int taskBatchSize){
        this.taskPool = taskPool;
        this.taskBatchSize = taskBatchSize;
        this.setName(name);
        this.start();
    }

    @Override
    public void run() {
        while(this.taskPool.isOpen()) {
            try {
               List<Task> tasks = this.taskPool.awaitAndPopTasks(this.taskBatchSize);
               tasks.forEach(Task::execute);
            } catch (InterruptedException | UsedWhenClosedException e) {
                e.printStackTrace();
            }
        }
    }

}
