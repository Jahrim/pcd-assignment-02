package assignment02.concurrency.masterworker.master;

import assignment02.concurrency.masterworker.taskpool.Task;
import assignment02.concurrency.masterworker.worker.Worker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Collectors;

/**
 * Models an abstract master.
 */
public abstract class AbstractMaster implements Master {

    /** The workers controlled by this master. */
    protected final List<Worker> workers = new ArrayList<>();

    @Override
    public AbstractMaster syncExecute(List<Task> tasks) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(tasks.size());
        List<Task> syncTasks = tasks.stream()
                                    .map( (task) -> (Task)(() -> { task.execute(); latch.countDown(); }))
                                    .collect(Collectors.toList());
        this.distributeTasks(syncTasks);
        latch.await();
        return this;
    }

    @Override
    public AbstractMaster asyncExecute(List<Task> tasks) { return this.distributeTasks(tasks); }

    /**
     * Distributes the specified tasks equally among all workers.
     * @param tasks the specified tasks
     * @return this
     */
    protected abstract AbstractMaster distributeTasks(final List<Task> tasks);

}
