package assignment02.concurrency.masterworker.master;

import assignment02.concurrency.masterworker.taskpool.Task;
import assignment02.concurrency.masterworker.taskpool.TaskPool;
import assignment02.concurrency.masterworker.worker.Worker;
import assignment02.util.exception.UsedWhenClosedException;

import java.util.List;

/**
 * Models a master whose job is to distribute tasks among a set of workers.
 * The workers used by this master share the same task pool.
 */
public class SinglePoolMaster extends AbstractMaster {
    private final TaskPool taskPool;

    public SinglePoolMaster(int nWorkers) { this(nWorkers, 50); }
    public SinglePoolMaster(int nWorkers, int taskBatchSize) {
        super();
        this.taskPool = new TaskPool();
        for (int i = 0; i < nWorkers; i++){
            this.workers.add(new Worker(this.taskPool, taskBatchSize));
        }
    }

    @Override
    protected SinglePoolMaster distributeTasks(final List<Task> tasks) {
        try { this.taskPool.addTasks(tasks); } catch (UsedWhenClosedException e) { e.printStackTrace(); }
        return this;
    }

    @Override
    public SinglePoolMaster killWorkers() {
        try { this.taskPool.close(); } catch (UsedWhenClosedException e) { e.printStackTrace(); }
        return this;
    }
}
