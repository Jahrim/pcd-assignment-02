package assignment02.concurrency.masterworker.taskpool;

/**
 * Models a task to be executed.
 */
@FunctionalInterface
public interface Task {
    /** Executes this task. */
    void execute();
}
