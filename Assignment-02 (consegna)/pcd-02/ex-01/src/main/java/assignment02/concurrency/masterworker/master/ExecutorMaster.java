package assignment02.concurrency.masterworker.master;

import assignment02.concurrency.masterworker.taskpool.Task;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Models a master whose job is to distribute tasks among a set of workers.
 * The distribution logic is delegated to a java executor.
 */
public class ExecutorMaster implements Master {

    private final static long DEFAULT_TIMEOUT_MS = 500000;
    private final ExecutorService executor;

    public ExecutorMaster(final ExecutorService executor){ this.executor = executor; }

    @Override
    public Master syncExecute(List<Task> tasks) throws InterruptedException {
        List<Future<?>> futures = tasks.stream().map(task -> (Runnable)task::execute).map(this.executor::submit).collect(Collectors.toList());
        futures.forEach(future -> { try { future.get(); } catch (Exception e) { e.printStackTrace(); } });
        return this;
    }

    @Override
    public Master asyncExecute(List<Task> tasks) {
        tasks.stream().map(task -> (Runnable)task::execute).forEach(this.executor::execute);
        return this;
    }

    @Override
    public Master killWorkers() {
        this.executor.shutdown();
        try {
            boolean hasTimeoutExpired = !this.executor.awaitTermination(ExecutorMaster.DEFAULT_TIMEOUT_MS, TimeUnit.MILLISECONDS);
            if (hasTimeoutExpired) { throw new InterruptedException("Computation took too long."); }
        } catch (InterruptedException e) { e.printStackTrace(); }
        return this;
    }
}
