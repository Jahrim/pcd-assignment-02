package assignment02.util.data;

/**
 * Models a pair of objects.
 * @param <K> the type of the first object
 * @param <V> the type of the second object
 */
public class Pair<K, V> {

    /** The first element of this pair. */
    public final K _1;
    /** The second element of this pair. */
    public final V _2;

    /**
     * @param first the first specified object
     * @param second the second specified object
     * @param <K> the type of the first specified object
     * @param <V> the type of the second specified object
     * @return a new pair of the specified objects.
     */
    public static <K,V> Pair<K,V> of(final K first, final V second){ return new Pair<>(first, second); }

    private Pair(final K first, final V second){
        this._1 = first;
        this._2 = second;
    }
}
