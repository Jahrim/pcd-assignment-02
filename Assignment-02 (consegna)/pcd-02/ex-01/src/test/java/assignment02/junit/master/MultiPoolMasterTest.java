package assignment02.junit.master;

import assignment02.concurrency.masterworker.master.MultiPoolMaster;

public class MultiPoolMasterTest extends MasterTest {
    protected MultiPoolMasterTest() {
        super(new MultiPoolMaster(Runtime.getRuntime().availableProcessors() + 1));
    }
}
