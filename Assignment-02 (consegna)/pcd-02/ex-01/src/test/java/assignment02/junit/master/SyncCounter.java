package assignment02.junit.master;

/** [Monitor] Models a thread-safe counter. */
public class SyncCounter {
    private int count;

    public SyncCounter(int init) { this.count = init; }

    /**
     * Increments this counter.
     * @return this.
     */
    public synchronized SyncCounter increment(){
        this.count++;
        return this;
    }

    /** @return this counter. */
    public synchronized int getCount() { return this.count; }

    /**
     * Resets this counter to the specified value.
     * @param init the specified value
     * @return this
     */
    public synchronized SyncCounter reset(int init) {
        this.count = init;
        return this;
    }
}
