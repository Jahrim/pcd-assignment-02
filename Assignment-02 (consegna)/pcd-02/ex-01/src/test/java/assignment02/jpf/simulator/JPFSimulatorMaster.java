//package assignment01.jpf.simulator;
//
//import assignment01.controller.simulator.master.AbstractSimulatorMaster;
//import assignment01.controller.simulator.master.SimulatorSinglePoolMaster;
//
//import assignment01.controller.simulator.taskpool.SimulatorTask;
//import gov.nasa.jpf.vm.Verify;
//
//import java.util.List;
//
//public class JPFSimulatorMaster extends SimulatorSinglePoolMaster {
//    public JPFSimulatorMaster(int nWorkers) {
//        super(nWorkers);
//    }
//
//    @Override
//    protected AbstractSimulatorMaster distributeTasks(List<SimulatorTask> tasks) {
//        List<SimulatorTask> jpfTasks = tasks.stream().map((task) -> (SimulatorTask) () -> {
//            Verify.beginAtomic();
//            task.execute();
//            Verify.endAtomic();
//        }).toList();
//        return super.distributeTasks(tasks);
//    }
//}
