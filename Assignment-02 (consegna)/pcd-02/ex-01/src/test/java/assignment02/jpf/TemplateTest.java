package assignment02.jpf;

public class TemplateTest {

    public static void main(String[] args) {
        new Thread( () -> System.out.println("Hello from Thread 1") ).start();
        new Thread( () -> System.out.println("Hello from Thread 2") ).start();
    }

}
