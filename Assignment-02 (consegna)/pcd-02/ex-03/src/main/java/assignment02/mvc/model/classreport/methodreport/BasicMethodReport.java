package assignment02.mvc.model.classreport.methodreport;

import assignment02.mvc.model.AbstractScopedProjectElement;
import com.github.javaparser.ast.AccessSpecifier;

import java.util.*;

/** Basic implementation for a method report. */
public class BasicMethodReport extends AbstractScopedProjectElement implements MethodReport {

    private String returnTypeName = "NOT_SET";
    private final List<MethodParameter> parameters = new LinkedList<>();
    private int codeBeginLine = -1;
    private int codeEndLine = -1;

    @Override
    public String getReturnTypeName() { return this.returnTypeName; }
    @Override
    public List<MethodParameter> getParameters() { return this.parameters; }
    @Override
    public int getCodeBeginLine() { return this.codeBeginLine; }
    @Override
    public int getCodeEndLine() { return this.codeEndLine; }

    @Override
    public BasicMethodReport setReturnTypeName(String typeName) { this.returnTypeName = typeName; return this; }
    @Override
    public BasicMethodReport addParameters(Collection<MethodParameter> parameters) { this.parameters.addAll(parameters); return this; }
    @Override
    public BasicMethodReport setCodeBeginLine(int codeBeginLine) { this.codeBeginLine = codeBeginLine; return this; }
    @Override
    public BasicMethodReport setCodeEndLine(int codeEndLine) { this.codeEndLine = codeEndLine; return this; }

    @Override
    public String toString() {
        return "Method[" +
                this.getAccessSpecifier().asString() + (this.getAccessSpecifier() == AccessSpecifier.PACKAGE_PRIVATE ? "" : " ") +
                this.getName() + "(" +
                this.getParameters().stream().map(Object::toString).reduce((s1, s2) -> s1 + ", " + s2).orElse("") +
                "): " + this.getReturnTypeName() +
                " {" + this.getCodeBeginLine() + ":" + this.getCodeEndLine() + "}" +
        "]";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BasicMethodReport that = (BasicMethodReport) o;
        return codeBeginLine == that.codeBeginLine && codeEndLine == that.codeEndLine && Objects.equals(returnTypeName, that.returnTypeName) && Objects.equals(parameters, that.parameters);
    }
    @Override
    public int hashCode() { return Objects.hash(super.hashCode(), returnTypeName, parameters, codeBeginLine, codeEndLine); }

    /* ### CASTING SUPER METHODS ######################################################################################################################### */
    @Override public BasicMethodReport setName(String name) { return (BasicMethodReport) super.setName(name); }
    @Override public BasicMethodReport setFullName(String fullName) { return (BasicMethodReport) super.setFullName(fullName); }
    @Override public BasicMethodReport setAccessSpecifier(AccessSpecifier accessSpecifier) { return (BasicMethodReport) super.setAccessSpecifier(accessSpecifier); }
    @Override public BasicMethodReport addParameters(MethodParameter... parameters) { return (BasicMethodReport) MethodReport.super.addParameters(parameters); }
    @Override public BasicMethodReport showContent() { return (BasicMethodReport) MethodReport.super.showContent(); }

}
