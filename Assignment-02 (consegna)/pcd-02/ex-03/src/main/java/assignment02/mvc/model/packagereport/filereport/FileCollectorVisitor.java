package assignment02.mvc.model.packagereport.filereport;

import assignment02.mvc.model.classreport.BasicClassReport;
import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.classreport.fieldreport.BasicFieldReport;
import assignment02.mvc.model.classreport.methodreport.BasicMethodParameter;
import assignment02.mvc.model.classreport.methodreport.BasicMethodReport;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.Node;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.util.stream.Collectors;

/** Visitor that collects a file report produced by parsing a specific java file. */
class FileCollectorVisitor extends VoidVisitorAdapter<FileReport> {

    @Override
    public void visit(PackageDeclaration node, FileReport fileReport) {
        fileReport.setFullName(node.getNameAsString());
        fileReport.setName(node.getName().getIdentifier());
        super.visit(node, fileReport);
    }

    @Override
    public void visit(ClassOrInterfaceDeclaration node, FileReport fileReport) {
        Node parentNode = node.getParentNode().orElseThrow();
        ClassReport classReport = new BasicClassReport()
                                        .setName(node.getNameAsString())
                                        .setAccessSpecifier(node.getAccessSpecifier())
                                        .setIsInterface(node.isInterface());
        if (parentNode instanceof CompilationUnit){                             //if parent is root then add to file report
            fileReport.addClasses(
                    classReport.setFullName(fileReport.getFullName() + "." + node.getNameAsString())
            );
        } else if (parentNode instanceof ClassOrInterfaceDeclaration) {         //if parent is class then add to super class
            ClassOrInterfaceDeclaration parentClass = (ClassOrInterfaceDeclaration) parentNode;
            ClassReport parentClassReport = fileReport.searchClass(parentClass.getNameAsString()).orElseThrow();
            parentClassReport.addInnerClasses(
                    classReport.setFullName(parentClassReport.getFullName() + "." + node.getNameAsString())
            );
        }
        super.visit(node, fileReport);
    }

    @Override
    public void visit(MethodDeclaration node, FileReport fileReport) {
        Node parentNode = node.getParentNode().orElseThrow();
        if (parentNode instanceof ClassOrInterfaceDeclaration) {    //if not a child of an anonymous class
            ClassOrInterfaceDeclaration parentClass = (ClassOrInterfaceDeclaration) parentNode;
            ClassReport parentClassReport = fileReport.searchClass(parentClass.getNameAsString()).orElseThrow();
            parentClassReport.addMethods(
                    new BasicMethodReport()
                            .setName(node.getNameAsString())
                            .setFullName(parentClassReport.getFullName() + "." + node.getNameAsString())
                            .setAccessSpecifier(node.getAccessSpecifier())
                            .setReturnTypeName(node.getTypeAsString())
                            .addParameters(node.getParameters().stream().map(parameter ->
                                    new BasicMethodParameter().setName(parameter.getNameAsString())
                                            .setTypeName(parameter.getTypeAsString())
                            ).collect(Collectors.toList()))
                            .setCodeBeginLine(node.getBegin().orElseThrow().line)
                            .setCodeEndLine(node.getEnd().orElseThrow().line)
            );
        }
        super.visit(node, fileReport);
    }

    @Override
    public void visit(FieldDeclaration node, FileReport fileReport) {
        Node parentNode = node.getParentNode().orElseThrow();
        if (parentNode instanceof ClassOrInterfaceDeclaration) {    //if not a child of an anonymous class
            ClassOrInterfaceDeclaration parentClass = (ClassOrInterfaceDeclaration) parentNode;
            ClassReport parentClassReport = fileReport.searchClass(parentClass.getNameAsString()).orElseThrow();
            parentClassReport.addFields(
                    node.getVariables().stream().map(variable ->
                            new BasicFieldReport()
                                    .setName(variable.getNameAsString())
                                    .setFullName(parentClassReport.getFullName() + "." + variable.getNameAsString())
                                    .setAccessSpecifier(node.getAccessSpecifier())
                                    .setTypeName(node.getElementType().asString())
                    ).collect(Collectors.toList())
            );
        }
        super.visit(node, fileReport);
    }
}
