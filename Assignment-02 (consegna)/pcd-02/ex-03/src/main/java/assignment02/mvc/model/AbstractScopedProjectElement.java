package assignment02.mvc.model;

import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.classreport.fieldreport.FieldReport;
import assignment02.mvc.model.classreport.methodreport.MethodReport;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.github.javaparser.ast.AccessSpecifier;

import java.util.Objects;

/** Models an abstract scoped project element. */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ClassReport.class, name = "ClassReport"),
        @JsonSubTypes.Type(value = MethodReport.class, name = "MethodReport"),
        @JsonSubTypes.Type(value = FieldReport.class, name = "FieldReport")
})
public abstract class AbstractScopedProjectElement extends AbstractProjectElement implements ScopedProjectElement {

    private AccessSpecifier accessSpecifier = null;

    @Override
    public AccessSpecifier getAccessSpecifier() { return this.accessSpecifier; }
    @Override
    public AbstractScopedProjectElement setAccessSpecifier(AccessSpecifier accessSpecifier) { this.accessSpecifier = accessSpecifier; return this; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractScopedProjectElement)) return false;
        if (!super.equals(o)) return false;
        AbstractScopedProjectElement that = (AbstractScopedProjectElement) o;
        return accessSpecifier == that.accessSpecifier;
    }
    @Override
    public int hashCode() { return Objects.hash(super.hashCode(), accessSpecifier); }

    /* ### CASTING SUPER METHODS ######################################################################################################################### */
    @Override public AbstractScopedProjectElement setName(String name) { return (AbstractScopedProjectElement) super.setName(name); }
    @Override public AbstractScopedProjectElement setFullName(String fullName) { return (AbstractScopedProjectElement) super.setFullName(fullName); }

}
