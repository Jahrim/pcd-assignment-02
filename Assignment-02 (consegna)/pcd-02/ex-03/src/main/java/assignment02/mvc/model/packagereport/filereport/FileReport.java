package assignment02.mvc.model.packagereport.filereport;

import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.packagereport.PackageReport;
import assignment02.mvc.model.packagereport.PartialPackageReport;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;

/** Models a partial package report built from a single java file. */
@JsonDeserialize(as = FileReport.class)
public class FileReport extends PartialPackageReport {

    /**
     * @param file the specified file
     * @return a partial package report obtained from parsing the specified java file.
     */
    public static FileReport fromFile(File file) throws FileNotFoundException {
        FileReport fileReport = new FileReport();
        CompilationUnit ast = StaticJavaParser.parse(file);
        new FileCollectorVisitor().visit(ast, fileReport);
        return fileReport;
    }

    /* ### CASTING SUPER METHODS ######################################################################################################################### */
    @Override public FileReport setName(String name) { return (FileReport) super.setName(name); }
    @Override public FileReport setFullName(String fullName) { return (FileReport) super.setFullName(fullName); }
    @Override public FileReport setIsProject(boolean isProject) { return (FileReport) super.setIsProject(isProject); }
    @Override public FileReport addSubpackages(Collection<PackageReport> packages) { return (FileReport) super.addSubpackages(packages); }
    @Override public FileReport addSubpackages(PackageReport... packages) { return (FileReport) super.addSubpackages(packages); }
    @Override public FileReport addClasses(Collection<ClassReport> classes) { return (FileReport) super.addClasses(classes); }
    @Override public FileReport addClasses(ClassReport... classes) { return (FileReport) super.addClasses(classes); }
    @Override public FileReport showContent() { return (FileReport) super.showContent(); }

}
