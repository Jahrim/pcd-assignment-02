package assignment02.mvc.model.classreport.methodreport;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/** Models a parameter in a method. */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BasicMethodParameter.class, name = "BasicMethodParameter")
})
@JsonDeserialize(as = BasicMethodParameter.class)
public interface MethodParameter {

    /** @return the name of this parameter. */
    @JsonProperty("name") String getName();
    /** @return the name of the type of this parameter. */
    @JsonProperty("typeName") String getTypeName();

    /**
     * Sets the name of this parameter to the specified name.
     * @param name the specified name
     * @return this
     */
    MethodParameter setName(String name);
    /**
     * Sets the name of the type of this parameter to the specified name.
     * @param typeName the specified name
     * @return this
     */
    MethodParameter setTypeName(String typeName);

}
