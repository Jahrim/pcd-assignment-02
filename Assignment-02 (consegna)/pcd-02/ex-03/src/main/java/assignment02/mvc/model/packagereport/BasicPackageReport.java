package assignment02.mvc.model.packagereport;

import assignment02.mvc.model.AbstractProjectElement;
import assignment02.mvc.model.classreport.ClassReport;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/** Basic implementation for a package report. */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = PartialPackageReport.class, name = "PartialPackageReport")
})
public class BasicPackageReport extends AbstractProjectElement implements PackageReport {

    protected boolean isProject = false;
    protected final List<PackageReport> childSubpackages = new LinkedList<>();
    protected final List<ClassReport> childSubclasses = new LinkedList<>();

    @Override
    public boolean isProject() { return this.isProject; }
    @Override
    public List<PackageReport> getChildSubpackages() { return this.childSubpackages; }
    @Override
    public List<ClassReport> getChildClasses() { return this.childSubclasses; }

    @Override
    public BasicPackageReport setIsProject(boolean isProject) { this.isProject = isProject; return this; }
    @Override
    public BasicPackageReport addSubpackages(Collection<PackageReport> packages) { this.childSubpackages.addAll(packages); return this; }
    @Override
    public BasicPackageReport addClasses(Collection<ClassReport> classes) { this.childSubclasses.addAll(classes); return this; }

    @Override
    public String toString() {
        String packageContent = (
                this.getChildClasses().stream().map(Object::toString).reduce((s1,s2) -> s1 + "\n" + s2).map(s -> "\n" + s).orElse("") +
                this.getChildSubpackages().stream().map(Object::toString).reduce((s1,s2) -> s1 + "\n" + s2).map(s -> "\n" + s).orElse("")
        ).replace("\n", "\n\t");

        return ((this.isProject) ? "Project[" + this.getFullName() : "Package[" + this.getName()) +
                " {" + packageContent + (packageContent.isEmpty() ? " " : "\n") + "}]";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BasicPackageReport)) return false;
        if (!super.equals(o)) return false;
        BasicPackageReport that = (BasicPackageReport) o;
        return isProject == that.isProject && Objects.equals(childSubpackages, that.childSubpackages) && Objects.equals(childSubclasses, that.childSubclasses);
    }
    @Override
    public int hashCode() { return Objects.hash(super.hashCode(), isProject, childSubpackages, childSubclasses); }

    /* ### CASTING SUPER METHODS ######################################################################################################################### */
    @Override public BasicPackageReport setName(String name) { return (BasicPackageReport) super.setName(name); }
    @Override public BasicPackageReport setFullName(String fullName) { return (BasicPackageReport) super.setFullName(fullName); }
    @Override public BasicPackageReport addSubpackages(PackageReport... packages) { return (BasicPackageReport) PackageReport.super.addSubpackages(packages); }
    @Override public BasicPackageReport addClasses(ClassReport... classes) { return (BasicPackageReport) PackageReport.super.addClasses(classes); }
    @Override public BasicPackageReport showContent() { return (BasicPackageReport) PackageReport.super.showContent(); }
}
