package assignment02.mvc.model;

import assignment02.mvc.model.packagereport.PackageReport;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Objects;

/** Models an abstract project element. */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = PackageReport.class, name = "PackageReport"),
        @JsonSubTypes.Type(value = AbstractScopedProjectElement.class, name = "AbstractScopedProjectElement")
})
public abstract class AbstractProjectElement implements ProjectElement {

    private String name = "NOT_SET";
    private String fullName = "NOT_SET";

    @Override
    public String getName() { return this.name; }
    @Override
    public String getFullName() { return this.fullName; }

    @Override
    public AbstractProjectElement setName(String name) { this.name = name; return this; }
    @Override
    public AbstractProjectElement setFullName(String fullName) { this.fullName = fullName; return this; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractProjectElement)) return false;
        AbstractProjectElement that = (AbstractProjectElement) o;
        return Objects.equals(name, that.name) && Objects.equals(fullName, that.fullName);
    }
    @Override
    public int hashCode() { return Objects.hash(name, fullName); }
}
