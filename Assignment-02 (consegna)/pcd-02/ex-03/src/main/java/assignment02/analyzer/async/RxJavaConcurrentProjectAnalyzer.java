package assignment02.analyzer.async;

import assignment02.mvc.model.ProjectElement;
import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.packagereport.PackageReport;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Observable;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Models a project analyzer that allows you to search for information inside a
 * certain java project. Based on Rx Java.
 */
public interface RxJavaConcurrentProjectAnalyzer {

	/**
	 * @param file the file where the requested interface is defined
	 * @param name the name of the requested interface
	 * @return a singleton observable containing the report about the requested interface.
	 */
	Maybe<ClassReport> getInterfaceReport(File file, String name) throws FileNotFoundException;

	/**
	 * @param file the file where the requested class is defined
	 * @param name the name of the requested class
	 * @return a singleton observable containing the report about the requested class.
	 */
	Maybe<ClassReport> getClassReport(File file, String name) throws FileNotFoundException;

	/**
	 * @param directory the directory representing the requested package
	 * @return a singleton observable containing the report about the requested package.
	 */
	Maybe<PackageReport> getPackageReport(File directory);

	/**
	 * @param directory the directory representing the requested project
	 * @return a singleton observable containing the report about the requested project.
	 */
	Maybe<PackageReport> getProjectReport(File directory);
	
	/**
	 * Async method to analyze a project, extracting every discovered project element.
	 * @param directory the directory representing the requested project
	 * @return an observable containing all the project elements discovered in the requested project.
	 */
	Observable<ProjectElement> analyzeProject(File directory);

}
