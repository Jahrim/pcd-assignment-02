package assignment02.analyzer.async;

import assignment02.PathManager;
import assignment02.mvc.model.ProjectElement;
import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.packagereport.PackageReport;
import assignment02.mvc.model.packagereport.PartialPackageReport;
import assignment02.mvc.model.packagereport.filereport.FileReport;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.ObservableEmitter;
import io.reactivex.rxjava3.schedulers.Schedulers;

import javax.annotation.Nullable;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

/** Basic implementation of a concurrent project analyzer. */
public class BasicRxJavaConcurrentProjectAnalyzer implements RxJavaConcurrentProjectAnalyzer {

    @Override
    public Maybe<ClassReport> getInterfaceReport(File file, String name) throws FileNotFoundException {
        return Maybe.fromOptional(
            FileReport.fromFile(file).searchClass(name).filter(ClassReport::isInterface)
        ).subscribeOn(Schedulers.computation());
    }

    @Override
    public Maybe<ClassReport> getClassReport(File file, String name) throws FileNotFoundException {
        return Maybe.fromOptional(
            FileReport.fromFile(file).searchClass(name).filter(classReport -> !classReport.isInterface())
        ).subscribeOn(Schedulers.computation());
    }

    @Override
    public Maybe<PackageReport> getPackageReport(File directory) {
        return PathManager.getManager(directory.toPath()).childFilesOf().isEmpty()
                ? Maybe.empty()
                : this.getPackageReport(directory, null, false, null).firstElement();
    }

    @Override
    public Maybe<PackageReport> getProjectReport(File directory) {
        return PathManager.getManager(directory.toPath()).childFilesOf().isEmpty()
                ? Maybe.empty()
                : this.getPackageReport(directory, null, true, null).firstElement();
    }

    @Override
    public Observable<ProjectElement> analyzeProject(File directory) {
        return Observable.create(emitter -> getPackageReport(directory, null, true, emitter).subscribe());
    }

    /**
     * @param directory the specified directory
     * @param parentPackageFullName the name of the parent package, null if this package has no parent
     * @param isProject true if this package report is a project report, false otherwise
     * @param discoveredElements an observable which discovered project elements will be emitted to
     * @return an observable containing the package report of the specified directory, considering the
     *         specified name as the name of the parent package.
     */
    private Observable<PackageReport> getPackageReport(
        File directory,
        @Nullable final String parentPackageFullName,
        boolean isProject,
        @Nullable ObservableEmitter<ProjectElement> discoveredElements
    ){
        final List<File> childFiles = PathManager.getManager(directory.toPath()).childFilesOf();
        final String packageFullName = parentPackageFullName == null ? "" : (parentPackageFullName + ".") + directory.getName();

        return Observable.fromIterable(childFiles)
                .subscribeOn(Schedulers.computation())
                //extract file reports from files that are direct children of the specified directory and discover their project elements
                .filter(File::isFile)
                .flatMap(file ->
                    Observable.just(file)
                              .subscribeOn(Schedulers.computation())
                              .<PartialPackageReport>map(FileReport::fromFile)
                              .doOnNext(fileReport -> fileReport.getChildClasses().forEach(
                                  classReport -> this.discoverElementsOnClass(classReport, discoveredElements))
                              )
                )
                //merge them into a package report and finalize package report
                .reduce(PartialPackageReport::merge)
                .defaultIfEmpty(new PartialPackageReport())
                .<PackageReport>map(partialPackageReport ->
                        partialPackageReport.seal()
                                            .setIsProject(isProject)
                                            .setName(directory.getName())
                                            .setFullName(isProject ? directory.getAbsolutePath() : packageFullName)
                )
                //extract package reports from directories that are direct children of the specified directory and add those subpackages to the package report
                .flatMap(packageReport ->
                    Observable.fromIterable(childFiles)
                              .subscribeOn(Schedulers.computation())
                              .filter(File::isDirectory)
                              .flatMap(dir -> this.getPackageReport(dir, packageFullName, false, discoveredElements))
                              .toList()
                              .map(packageReport::addSubpackages)
                )
                //discover this package report and close the stream of discovered elements if this package report is the project report
                .doOnSuccess(completedPackageReport -> {
                    if (discoveredElements != null) {
                        discoveredElements.onNext(completedPackageReport);
                        if (isProject) { discoveredElements.onComplete(); }
                    }
                })
                .toObservable();
    }

    /**
     * Discovers all elements of the specified class.
     * @param classReport the report of the specified class
     * @param discoveredElements an observable which the discovered project elements will be emitted to
     */
    private void discoverElementsOnClass(final ClassReport classReport, ObservableEmitter<ProjectElement> discoveredElements){
        classReport.getChildInnerClasses().forEach(subclass -> discoverElementsOnClass(subclass, discoveredElements));
        classReport.getFields().forEach(e -> { if (discoveredElements != null) { discoveredElements.onNext(e); }});
        classReport.getMethods().forEach(e -> { if (discoveredElements != null) { discoveredElements.onNext(e); }});
        if (discoveredElements != null) { discoveredElements.onNext(classReport); }
    }

}
