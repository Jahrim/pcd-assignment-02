/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package assignment02.manual.analyzer;

import assignment02.PathManager;
import assignment02.analyzer.sync.BasicSequentialProjectAnalyzer;
import assignment02.mvc.model.classreport.ClassReport;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.NoSuchElementException;

/** An example on how to use a sequential project analyzer. */
public class TrySequentialClassAnalysis {

    private static final Path pathToFileToParse = PathManager.getManager("src/test/java").path(
            "assignment02",
            "parse",
            "examplepackage",
            "ExampleClass.java"
    );

    public static void main(String[] args) {
        System.out.println("### ANALYZING PROJECT ############");

        System.out.println("\nSELECTED DIRECTORY: ");
        System.out.println(pathToFileToParse.toAbsolutePath());

        try {
            System.out.println("\nPRODUCING CLASS REPORT...");
            ClassReport classReport = new BasicSequentialProjectAnalyzer()
                                            .getClassReport(pathToFileToParse.toFile(), "ExampleClass")
                                            .orElseThrow();
            classReport.showContent();

            System.out.println("\nPRODUCING INTERFACE REPORT...");
            ClassReport interfaceReport = new BasicSequentialProjectAnalyzer()
                                            .getInterfaceReport(pathToFileToParse.toFile(), "ExampleInterface")
                                            .orElseThrow();
            interfaceReport.showContent();
        } catch (NoSuchElementException | FileNotFoundException e) {
            e.printStackTrace();
            System.err.println("Caused by this path: ");
            System.err.println(pathToFileToParse.toAbsolutePath());
        }
    }

}
