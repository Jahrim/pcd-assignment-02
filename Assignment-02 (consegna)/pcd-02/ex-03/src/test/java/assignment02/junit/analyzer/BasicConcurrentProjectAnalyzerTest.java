package assignment02.junit.analyzer;

import assignment02.PathManager;
import assignment02.analyzer.async.BasicRxJavaConcurrentProjectAnalyzer;
import assignment02.analyzer.sync.BasicSequentialProjectAnalyzer;
import assignment02.mvc.model.ProjectElement;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

public class BasicConcurrentProjectAnalyzerTest {

    private final File projectFolder = PathManager.getManager().path().toFile();
    private final BasicRxJavaConcurrentProjectAnalyzer concurrentProjectAnalyzer = new BasicRxJavaConcurrentProjectAnalyzer();
    private final BasicSequentialProjectAnalyzer sequentialProjectAnalyzer = new BasicSequentialProjectAnalyzer();

    @Test
    void getProjectReportTest(){
        this.concurrentProjectAnalyzer.getProjectReport(projectFolder)
            .blockingSubscribe(projectReport ->
                assertEquals(
                    projectReport.toString().length(),
                    this.sequentialProjectAnalyzer.getProjectReport(projectFolder).orElseThrow().toString().length()
                )
            );
    }

    @Test
    void analyzeProjectTest(){
        this.concurrentProjectAnalyzer.analyzeProject(projectFolder).blockingSubscribe(element -> {
            if (element.getProjectElementType() == ProjectElement.ProjectElementType.PROJECT){
                assertEquals(
                    element.toString().length(),
                    this.sequentialProjectAnalyzer.getProjectReport(projectFolder).orElseThrow().toString().length()
                );
            }
        });
    }
}
