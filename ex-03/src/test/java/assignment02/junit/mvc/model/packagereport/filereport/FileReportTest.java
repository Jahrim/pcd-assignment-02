package assignment02.junit.mvc.model.packagereport.filereport;

import assignment02.PathManager;
import assignment02.mvc.model.packagereport.filereport.FileReport;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.nio.file.Path;

/** Test for the class FileReport. */
class FileReportTest {

    private final static Path exampleClassPath = PathManager.getManager("src/test/java").path(
        "assignment02",
        "parse",
        "examplepackage",
        "ExampleClass.java"
    );

    @Test void fromFileTest() {
        try {
            FileReport fileReport = FileReport.fromFile(exampleClassPath.toFile());
            FileReport expected = StaticExampleClassFileReport.get();
            assertEquals(expected, fileReport);
        } catch (FileNotFoundException e){
            e.printStackTrace();
            System.err.println("Caused by this path: ");
            System.err.println(exampleClassPath.toAbsolutePath());
        }
    }
}
