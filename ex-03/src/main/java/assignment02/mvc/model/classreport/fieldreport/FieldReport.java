package assignment02.mvc.model.classreport.fieldreport;
import assignment02.mvc.model.Report;
import assignment02.mvc.model.ScopedProjectElement;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.github.javaparser.ast.AccessSpecifier;

/** Models a report for a field of a certain class. */
@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		property = "type")
@JsonSubTypes({
		@JsonSubTypes.Type(value = BasicFieldReport.class, name = "BasicFieldReport")
})
@JsonDeserialize(as = BasicFieldReport.class)
public interface FieldReport extends ScopedProjectElement, Report {

	@Override
	default ProjectElementType getProjectElementType(){ return ProjectElementType.FIELD; }

	/** @return the name of the type of this field. */
	@JsonProperty("typeName") String getTypeName();

	/**
	 * Sets the name of the type of the field of this report to the specified name.
	 * @param typeName the specified name
	 * @return this
	 */
	FieldReport setTypeName(String typeName);

	/* ### CASTING SUPER METHODS ######################################################################################################################### */
	@Override FieldReport setName(String name);
	@Override FieldReport setFullName(String fullName);
	@Override FieldReport setAccessSpecifier(AccessSpecifier accessSpecifier);
	@Override default FieldReport showContent() { return (FieldReport) Report.super.showContent(); }

}
