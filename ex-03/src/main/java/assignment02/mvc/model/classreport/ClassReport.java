package assignment02.mvc.model.classreport;

import assignment02.mvc.model.classreport.fieldreport.FieldReport;
import assignment02.mvc.model.Report;
import assignment02.mvc.model.ScopedProjectElement;
import assignment02.mvc.model.classreport.methodreport.MethodReport;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.github.javaparser.ast.AccessSpecifier;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** Models a report for a java class or interface. */
@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		property = "type")
@JsonSubTypes({
		@JsonSubTypes.Type(value = BasicClassReport.class, name = "BasicClassReport")
})
@JsonDeserialize(as = BasicClassReport.class)
public interface ClassReport extends ScopedProjectElement, Report {

	@Override
	default ProjectElementType getProjectElementType(){
		return this.isInterface() ? ProjectElementType.INTERFACE : ProjectElementType.CLASS;
	}

	/** @return true if this is a report for an interface, false otherwise. */
	@JsonProperty("isInterface") boolean isInterface();
	/** @return a list of reports for the inner classes directly defined in this class. */
	@JsonProperty("childInnerClasses") List<ClassReport> getChildInnerClasses();
	/** @return a list of reports for the methods of this class. */
	@JsonProperty("methods") List<MethodReport> getMethods();
	/** @return a list of reports for the fields of this class. */
	@JsonProperty("fields") List<FieldReport> getFields();

	/**
	 * Sets the property that tells if this report is about an interface to the specified value.
	 * @param isInterface the specified value
	 * @return this
	 */
	ClassReport setIsInterface(boolean isInterface);
	/**
	 * Adds the specified inner class reports to the report of this class.
	 * @param subclasses the specified inner class reports
	 * @return this
	 */
	ClassReport addInnerClasses(Collection<ClassReport> subclasses);
	/**
	 * Adds the specified method reports to the report of this class.
	 * @param methods the specified method reports
	 * @return this
	 */
	ClassReport addMethods(Collection<MethodReport> methods);
	/**
	 * Adds the specified field reports to the report of this class.
	 * @param fields the specified field reports
	 * @return this
	 */
	ClassReport addFields(Collection<FieldReport> fields);

	/** Alternative to {@link #addInnerClasses(Collection)}. */
	default ClassReport addInnerClasses(ClassReport ...subclasses) {
		return this.addInnerClasses(Arrays.stream(subclasses).collect(Collectors.toList()));
	}
	/** Alternative to {@link #addMethods(Collection)}. */
	default ClassReport addMethods(MethodReport ...methods) {
		return this.addMethods(Arrays.stream(methods).collect(Collectors.toList()));
	}
	/** Alternative to {@link #addFields(Collection)}. */
	default ClassReport addFields(FieldReport ...fields) {
		return this.addFields(Arrays.stream(fields).collect(Collectors.toList()));
	}

	/* ### QUERY METHODS ################################################################################################################################# */
	/** @return a list of reports for all the inner classes of this class. */
	default List<ClassReport> searchAllInnerClasses(){
		return this.getChildInnerClasses()
				.stream()
				.flatMap(classReport -> Stream.concat(
						Stream.of(classReport),
						classReport.searchAllInnerClasses().stream()
				))
				.collect(Collectors.toList());
	}

	/* ### CASTING SUPER METHODS ######################################################################################################################### */
	@Override ClassReport setName(String name);
	@Override ClassReport setFullName(String fullName);
	@Override ClassReport setAccessSpecifier(AccessSpecifier accessSpecifier);
	@Override default ClassReport showContent() { return (ClassReport) Report.super.showContent(); }
}
