package assignment02.mvc.model.classreport.methodreport;

import assignment02.mvc.model.Report;
import assignment02.mvc.model.ScopedProjectElement;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.github.javaparser.ast.AccessSpecifier;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/** Models a report for a method of a certain class. */
@JsonTypeInfo(
		use = JsonTypeInfo.Id.NAME,
		property = "type")
@JsonSubTypes({
		@JsonSubTypes.Type(value = BasicMethodReport.class, name = "BasicMethodReport")
})
@JsonDeserialize(as = BasicMethodReport.class)
public interface MethodReport extends ScopedProjectElement, Report {

	@Override
	default ProjectElementType getProjectElementType(){ return ProjectElementType.METHOD; }

	/** @return the name of the return type of this method. */
	@JsonProperty("returnType") String getReturnTypeName();
	/** @return the parameters of this method. */
	@JsonProperty("parameters") List<MethodParameter> getParameters();
	/** @return the number of the line where this method starts in the file that defines it. */
	@JsonProperty("codeBeginLine") int getCodeBeginLine();
	/** @return the number of the line where this method ends in the file that defines it. */
	@JsonProperty("codeEndLine") int getCodeEndLine();

	/**
	 * Sets the name of the return type of the method of this report to the specified name.
	 * @param typeName the specified name
	 * @return this
	 */
	MethodReport setReturnTypeName(String typeName);
	/**
	 * Adds the specified method parameters to the report of this method.
	 * @param parameters the specified method parameters.
	 * @return this
	 */
	MethodReport addParameters(Collection<MethodParameter> parameters);
	/**
	 * Sets the line of code where the method of this report starts to the specified line of code.
	 * @param codeBeginLine the specified line of code
	 * @return this
	 */
	MethodReport setCodeBeginLine(int codeBeginLine);
	/**
	 * Sets the line of code where the method of this report ends to the specified line of code.
	 * @param codeEndLine the specified line of code
	 * @return this
	 */
	MethodReport setCodeEndLine(int codeEndLine);

	/** Alternative to {@link #addParameters(Collection)}. */
	default MethodReport addParameters(MethodParameter ...parameters) {
		return this.addParameters(Arrays.stream(parameters).collect(Collectors.toList()));
	}

	/* ### CASTING SUPER METHODS ######################################################################################################################### */
	@Override MethodReport setName(String name);
	@Override MethodReport setFullName(String fullName);
	@Override MethodReport setAccessSpecifier(AccessSpecifier accessSpecifier);
	@Override default MethodReport showContent() { return (MethodReport) Report.super.showContent(); }

}
