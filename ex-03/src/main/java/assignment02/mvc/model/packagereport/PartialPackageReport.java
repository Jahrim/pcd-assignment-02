package assignment02.mvc.model.packagereport;

import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.packagereport.filereport.FileReport;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Models a partial package report that has to be merged with other partial
 * package reports, in order to build a complete package report.
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = FileReport.class, name = "FileReport")
})
@JsonDeserialize(as = PartialPackageReport.class)
public class PartialPackageReport extends BasicPackageReport {

    /**
     * @param other the specified package
     * @return a new package result of the merging between this package and the specified package.
     */
    public PartialPackageReport merge(PartialPackageReport other){
        return new PartialPackageReport().setName(this.getName())
                                         .setFullName(this.getFullName())
                                         .addSubpackages(this.getChildSubpackages())
                                         .addSubpackages(other.getChildSubpackages())
                                         .addClasses(this.getChildClasses())
                                         .addClasses(other.getChildClasses());
    }
    /**
     * @param others the specified packages
     * @return a new package result of the merging between this package and the specified packages.
     */
    public PartialPackageReport mergeAll(Collection<PartialPackageReport> others){
        return Stream.concat(Stream.of(this), others.stream()).reduce(PartialPackageReport::merge).orElse(this);
    }
    /** Alternative for {@link #mergeAll(Collection)}. */
    public PartialPackageReport mergeAll(PartialPackageReport ...others){
        return this.mergeAll(Arrays.stream(others).collect(Collectors.toList()));
    }

    /**
     * @return a package report obtained by sealing this partial package report, avoiding further
     *         merging with other partial package reports.
     */
    public BasicPackageReport seal(){
        return new BasicPackageReport().setName(this.getName())
                                       .setFullName(this.getFullName())
                                       .addSubpackages(this.getChildSubpackages())
                                       .addClasses(this.getChildClasses());
    }


    /* ### CASTING SUPER METHODS ######################################################################################################################### */
    @Override public PartialPackageReport setName(String name) { return (PartialPackageReport) super.setName(name); }
    @Override public PartialPackageReport setFullName(String fullName) { return (PartialPackageReport) super.setFullName(fullName); }
    @Override public PartialPackageReport setIsProject(boolean isProject) { return (PartialPackageReport) super.setIsProject(isProject); }
    @Override public PartialPackageReport addSubpackages(Collection<PackageReport> packages) { return (PartialPackageReport) super.addSubpackages(packages); }
    @Override public PartialPackageReport addSubpackages(PackageReport... packages) { return (PartialPackageReport) super.addSubpackages(packages); }
    @Override public PartialPackageReport addClasses(Collection<ClassReport> classes) { return (PartialPackageReport) super.addClasses(classes); }
    @Override public PartialPackageReport addClasses(ClassReport... classes) { return (PartialPackageReport) super.addClasses(classes); }
    @Override public PartialPackageReport showContent() { return (PartialPackageReport) super.showContent(); }
}
