package assignment02.mvc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/** Models an element in a project. */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AbstractProjectElement.class, name = "AbstractProjectElement"),
        @JsonSubTypes.Type(value = ScopedProjectElement.class, name = "ScopedProjectElement")
})
public interface ProjectElement extends Comparable<ProjectElement> {

    /** Models the type of a specific project element. */
    enum ProjectElementType { PROJECT, PACKAGE, INTERFACE, CLASS, FIELD, METHOD }

    /** @return the type of this project element. */
    @JsonIgnore ProjectElementType getProjectElementType();
    /** @return the name of this project element. */
    @JsonProperty("name") String getName();
    /** @return the full name of this project element. */
    @JsonProperty("fullName") String getFullName();

    /**
     * Sets the name of this project element to the specified name.
     * @param name the specified name
     * @return this
     */
    ProjectElement setName(String name);
    /**
     * Sets the full name of this project element to the specified full name.
     * @param fullName the specified full name
     * @return this
     */
    ProjectElement setFullName(String fullName);

    @Override
    default int compareTo(ProjectElement that){
        return this.getProjectElementType() != that.getProjectElementType()
               ? this.getProjectElementType().compareTo(that.getProjectElementType())
               : this.getName().compareTo(that.getName());
    }
}
