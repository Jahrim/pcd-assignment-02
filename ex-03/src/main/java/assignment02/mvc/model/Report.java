package assignment02.mvc.model;

/** Models a report for a certain concept. */
public interface Report {

    /**
     * Prints this report.
     * @return this
     */
    default Report showContent(){ System.out.println(this); return this; }
}
