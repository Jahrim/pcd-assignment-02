package assignment02.mvc.view.components;

import assignment02.mvc.model.ProjectElement;
import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.packagereport.PackageReport;
import javafx.scene.control.TreeItem;

import java.util.Comparator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** Models a node containing a project element in a TreeView. */
public class ReportTreeItem extends TreeItem<ProjectElement> {

    public ReportTreeItem(final ProjectElement root){ this(root, true); }
    public ReportTreeItem(final ProjectElement root, boolean sorted){
        super(root);
        switch(root.getProjectElementType()) {
            case PROJECT:
            case PACKAGE:
                PackageReport thisPackageReport = (PackageReport) root;
                this.getChildren().addAll(
                        Stream.<ProjectElement>concat(
                                        thisPackageReport.getChildClasses().stream(),
                                        thisPackageReport.getChildSubpackages().stream()
                                )
                                .map(ReportTreeItem::new)
                                .collect(Collectors.toList())
                );
                break;
            case INTERFACE:
            case CLASS:
                ClassReport thisClassReport = (ClassReport) root;
                this.getChildren().addAll(
                        Stream.<ProjectElement>concat(Stream.concat(
                                        thisClassReport.getFields().stream(),
                                        thisClassReport.getMethods().stream()
                                ), thisClassReport.getChildInnerClasses().stream())
                                .map(ReportTreeItem::new)
                                .collect(Collectors.toList())
                );
                break;
            case FIELD: break;
            case METHOD: break;
            default: throw new IllegalArgumentException("ReportTreeItem::new : Invalid project element type specified as root.");
        }
        if (sorted) { this.getChildren().sort(Comparator.comparing(TreeItem::getValue)); }
    }

    @Override
    public int hashCode() { return super.hashCode(); }
    @Override
    public boolean equals(Object obj) { return super.equals(obj); }
}
