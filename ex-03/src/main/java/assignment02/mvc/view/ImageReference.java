package assignment02.mvc.view;

import assignment02.PathManager;
import javafx.scene.image.Image;

import java.net.MalformedURLException;

/** Models a reference to an image. */
public enum ImageReference {

    ProjectIcon_16x16("icons/project-icon.png", 16, 16),
    PackageIcon_16x16("icons/package-icon.png", 16, 16),
    InterfaceIcon_16x16("icons/interface-icon.png", 16, 16),
    ClassIcon_16x16("icons/class-icon.png", 16, 16),
    FieldIcon_16x16("icons/field-icon.png", 16, 16),
    MethodIcon_16x16("icons/method-icon.png", 16, 16);

    private final Image image;
    ImageReference(final String imagePath, final double width, final double height){
        String imageURL = "";
        try {
            PathManager imageManager = PathManager.getManager("src/main/resources/images");
            imageURL = imageManager.path(imagePath).toUri().toURL().toString();
        } catch (MalformedURLException e) { e.printStackTrace(); }
        this.image = new Image(imageURL, width, height, false, false);
    }

    /** @return the image bound to this image reference. */
    public Image image() { return this.image; }

}
