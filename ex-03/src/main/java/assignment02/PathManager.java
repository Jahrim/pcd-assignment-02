package assignment02;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** Models a path manager for files inside a certain directory. */
@FunctionalInterface
public interface PathManager {
    Path PROJECT_ROOT = Path.of("src/main/java");

    /** @return the path of the folder managed by this path manager. */
    Path getRoot();

    /** @return a path manager for the main folder of this project. */
    static PathManager getManager(){ return PathManager.getManager(PROJECT_ROOT); }
    /**
     * @param directory the specified directory
     * @return a path manager for the specified directory.
     */
    static PathManager getManager(Path directory){ return () -> directory; }
    /** Alternative for {@link #getManager(Path)} */
    static PathManager getManager(String directory){ return PathManager.getManager(Path.of(directory)); }

    /**
     * @param relativeURI the specified relative URI
     * @return the path of the specified relative URI inside this directory.
     */
    default Path path(String relativeURI) {
        return this.getRoot().resolve(relativeURI);
    }
    /**
     * @param pathNames the specified relative URI as a sequence of names
     * @return the path of the specified relative URI inside this directory.
     */
    default Path path(String ...pathNames) {
        Path resolvedPath = this.getRoot();
        for (String name: pathNames) { resolvedPath = resolvedPath.resolve(name); }
        return resolvedPath;
    }

    /**
     * @param directory the specified directory inside this directory
     * @return all files that are direct children of the specified directory.
     */
    default List<File> childFilesOf(Path directory){
        File[] filesInDirectory = directory.toFile().listFiles();
        return filesInDirectory == null ? List.of() : List.of(filesInDirectory);
    }

    /**
     * @param directory the specified directory inside this directory
     * @return all files contained in the specified directory and its subdirectories.
     */
    default List<File> allFilesOf(Path directory){
        return this.childFilesOf(directory)
                .stream()
                .flatMap(file -> Stream.concat(
                        Stream.of(file),
                        file.isDirectory() ? this.allFilesOf(file.toPath()).stream() : Stream.of()
                )).collect(Collectors.toList());
    }

    /** Alternative for {@link #childFilesOf(Path)} */
    default List<File> childFilesOf(String directory){ return this.childFilesOf(this.path(directory)); }
    /** Alternative for {@link #childFilesOf(Path)}.
     *  @apiNote return all files that are direct children of the directory managed by this path manager
     *           if no paths are specified. */
    default List<File> childFilesOf(String ...pathNames){ return this.childFilesOf(this.path(pathNames)); }
    /** Alternative for {@link #allFilesOf(Path)} */
    default List<File> allFilesOf(String directory){ return this.allFilesOf(this.path(directory)); }
    /** Alternative for {@link #allFilesOf(Path)}.
     *  @apiNote return all files contained in the directory managed by this path manager and its
     *           subdirectories if no paths are specified. */
    default List<File> allFilesOf(String ...pathNames){ return this.allFilesOf(this.path(pathNames)); }

}
