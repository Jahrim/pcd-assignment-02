package assignment02.manual;

import assignment02.PathManager;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.PackageDeclaration;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.visitor.VoidVisitorAdapter;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

/** An example of an application of the java parser library. */
public class TryJavaParser {
	private static final Path pathToFileToParse = PathManager.getManager("src/test/java").path(
			"assignment02",
			"parse",
			"examplepackage",
			"ExampleClass.java"
	);

	public static void main(String[] args) {
		System.out.println("### TRYING JAVA PARSER ###########");

		System.out.println("\nSELECTED FILE: ");
		System.out.println(pathToFileToParse.toAbsolutePath());

		try {
			System.out.println("\nPARSING FILE...");
			File fileToParse = pathToFileToParse.toFile();
			CompilationUnit cu = StaticJavaParser.parse(fileToParse);

			/* Prints all elements in */
			System.out.println("\nPRINTING FULL REPORT...");
			new FullPrintVisitor().visit(cu, null);

			/* Collects the name of all the methods found by the java parser. */
			System.out.println("\nCOLLECTING METHOD NAMES...");
			final List<String> methodNameCollector = new ArrayList<>();
			new MethodNameVisitor().visit(cu, methodNameCollector);
			methodNameCollector.forEach(System.out::println);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.err.println("Caused by this path: ");
			System.err.println(pathToFileToParse.toAbsolutePath());
		}
	}
}

/** Visitor that prints the name of each visited java element. */
class FullPrintVisitor extends VoidVisitorAdapter<Void> {
	public void visit(PackageDeclaration pd, Void collector) {
		super.visit(pd, collector);
		System.out.println("Package:\t\t" + pd.getNameAsString());
	}
	public void visit(ClassOrInterfaceDeclaration cd, Void collector) {
		super.visit(cd, collector);
		System.out.println("Class:  \t\t" + cd.getNameAsString());
	}
	/* Note: `public int a = 0, b = 1, c = 2;` is a single FieldDeclaration with multiple variables.
	 *       To access the info about a certain variable use FieldDeclaration.getVariable(index). */
	public void visit(FieldDeclaration fd, Void collector) {
		super.visit(fd, collector);
		ClassOrInterfaceDeclaration parentClass = (ClassOrInterfaceDeclaration) fd.getParentNode().orElseThrow();
		System.out.println(
				"Field:  \t\t" +
				parentClass.getNameAsString() + "." + fd.getVariable(0).getNameAsString() +		//name of the first variable in field declaration
				": " + fd.getElementType()														//type of all variables in field declaration
		);
	}
	public void visit(MethodDeclaration md, Void collector) {
		super.visit(md, collector);
		ClassOrInterfaceDeclaration parentClass = (ClassOrInterfaceDeclaration) md.getParentNode().orElseThrow();
		System.out.println(
				"Method: \t\t" +
				parentClass.getNameAsString() + "." + md.getNameAsString() +	//method name
				"(" +															//params: (name,type)
				md.getParameters().stream()
								  .map(param -> param.getNameAsString() + ": " + param.getTypeAsString())
								  .reduce((s1,s2) -> s1 + ", " + s2).orElse("") +
				")" +
				": " + md.getTypeAsString());									//return type
	}
}

/** Visitor that collects the names of visited methods. */
class MethodNameVisitor extends VoidVisitorAdapter<List<String>> {
	/* Note: to access the class of a certain method you can use MethodDeclaration.getParentNode()
	 *       which returns the parent node of this method, which is either a class or an interface. */
	public void visit(MethodDeclaration md, List<String> collector) {
		super.visit(md, collector);
		ClassOrInterfaceDeclaration parentClass = (ClassOrInterfaceDeclaration) md.getParentNode().orElseThrow();
		collector.add(parentClass.getNameAsString() + "." + md.getNameAsString());
	}
}
