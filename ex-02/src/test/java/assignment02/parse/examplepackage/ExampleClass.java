package assignment02.parse.examplepackage;

/* An example of a complex java file to try java parser on. */

@SuppressWarnings("All")
public class ExampleClass {
    private int privateField = 0;
    protected final String protectedFinalField = "b";
    public static final Object publicStaticFinalField = new Object();
    Double packagePrivateField = null;

    private void privateVoidMethod(){ this.privateField = 0; }
    protected final String protectedFinalStringMethod(){ return this.protectedFinalField; }
    public static Object publicStaticObjectMethod(){ return ExampleClass.publicStaticFinalField; }
    Double packagePrivateDoubleMethod(){ return this.packagePrivateField; }
    void methodWithParameters(int a, String b){ b.concat(String.valueOf(a)); }

    public static class PublicStaticSubclass {
        private final int fieldA1 = 0;
        public int methodA1(){
            return this.fieldA1;
        }
    }
    public class PublicSubclass {
        private final String fieldB1 = "b";
        public String methodB1(){ return this.fieldB1; }
    }
    class PackagePrivateSubclassWithExtension extends PublicStaticSubclass {
        private final Object fieldC1 = new Object();
        public Object methodC1(){ return this.fieldC1; }

        @Override
        public int methodA1() {
            return 0;
        }

        public class SubclassA {
            public class SubclassA1{ int fieldA1 = 0; }
            public class SubclassA2{ void methodA2(){} }
            public class SubclassA3{ class SubclassA3A{} }
        }
        public class SubclassB {
            public class SubclassB1{ String fieldB1 = "b"; }
            public class SubclassB2{ String methodB2(){ return ""; } }
            public class SubclassB3{ class SubclassB3A{} }
        }
    }
}

@SuppressWarnings("All")
class OuterClass {
    public final ExampleClass field = new ExampleClass();
}

@SuppressWarnings("All")
interface ExampleInterface {
    public final static int field1 = 0;
    ExampleClass interfaceMethod();
    default ExampleClass defaultInterfaceMethod(){
        //comment 1
        //comment 2
        //comment 3
        return new OuterClass().field;
    }
}
