package assignment02.junit.analyzer;

import assignment02.PathManager;
import assignment02.analyzer.async.BasicVertxConcurrentProjectAnalyzer;
import assignment02.analyzer.sync.BasicSequentialProjectAnalyzer;
import io.vertx.core.Vertx;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.io.File;

public class BasicConcurrentProjectAnalyzerTest {

    private final File projectFolder = PathManager.getManager().path().toFile();
    private final BasicVertxConcurrentProjectAnalyzer concurrentProjectAnalyzer = new BasicVertxConcurrentProjectAnalyzer(Vertx.vertx());
    private final BasicSequentialProjectAnalyzer sequentialProjectAnalyzer = new BasicSequentialProjectAnalyzer();

    @Test
    void getProjectReportTest(){
        this.concurrentProjectAnalyzer.getProjectReport(projectFolder).onSuccess(projectReport ->
            assertEquals(
                projectReport.toString().length(),
                this.sequentialProjectAnalyzer.getProjectReport(projectFolder).toString().length()
            )
        );
    }

    @Test
    void analyzeProjectTest(){
        this.concurrentProjectAnalyzer.analyzeProject(projectFolder, __ -> {}).getResult().onSuccess(projectReport ->
            assertEquals(
                projectReport.toString().length(),
                this.sequentialProjectAnalyzer.getProjectReport(projectFolder).toString().length()
            )
        );
    }
}
