package assignment02.mvc.model;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.github.javaparser.ast.AccessSpecifier;

/** Models a project element with a visibility scope. */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AbstractScopedProjectElement.class, name = "AbstractScopedProjectElement")
})
public interface ScopedProjectElement extends ProjectElement {

    /** @return the access specifier of this project element. */
    @JsonProperty("accessSpecifier") AccessSpecifier getAccessSpecifier();
    /**
     * Sets the access specifier of this project element to the specified access specifier.
     * @param accessSpecifier the specified access specifier
     * @return this
     */
    ScopedProjectElement setAccessSpecifier(AccessSpecifier accessSpecifier);

    /* ### CASTING SUPER METHODS ######################################################################################################################### */
    @Override ScopedProjectElement setName(String name);
    @Override ScopedProjectElement setFullName(String fullName);
    
}
