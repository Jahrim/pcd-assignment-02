package assignment02.mvc.model.classreport.fieldreport;

import assignment02.mvc.model.AbstractScopedProjectElement;
import com.github.javaparser.ast.AccessSpecifier;

import java.util.Objects;

/** Basic implementation for a field report. */
public class BasicFieldReport extends AbstractScopedProjectElement implements FieldReport {

    private String typeName = "NOT_SET";

    @Override
    public String getTypeName() { return this.typeName; }

    @Override
    public BasicFieldReport setTypeName(String typeName) { this.typeName = typeName; return this; }

    @Override
    public String toString() {
        return "Field[" +
                this.getAccessSpecifier().asString() + (this.getAccessSpecifier() == AccessSpecifier.PACKAGE_PRIVATE ? "" : " ") +
                this.getName() + ": " +
                this.getTypeName() +
        "]";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BasicFieldReport that = (BasicFieldReport) o;
        return Objects.equals(typeName, that.typeName);
    }
    @Override
    public int hashCode() { return Objects.hash(super.hashCode(), typeName); }

    /* ### CASTING SUPER METHODS ######################################################################################################################### */
    @Override public BasicFieldReport setName(String name) { return (BasicFieldReport) super.setName(name); }
    @Override public BasicFieldReport setFullName(String fullName) { return (BasicFieldReport) super.setFullName(fullName); }
    @Override public BasicFieldReport setAccessSpecifier(AccessSpecifier accessSpecifier) { return (BasicFieldReport) super.setAccessSpecifier(accessSpecifier); }
    @Override public BasicFieldReport showContent() { return (BasicFieldReport) FieldReport.super.showContent(); }

}
