package assignment02.mvc.model.classreport;

import assignment02.mvc.model.classreport.fieldreport.FieldReport;
import assignment02.mvc.model.classreport.methodreport.MethodReport;
import assignment02.mvc.model.AbstractScopedProjectElement;
import com.github.javaparser.ast.AccessSpecifier;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/** Basic implementation for a class report. */
public class BasicClassReport extends AbstractScopedProjectElement implements ClassReport {

    private boolean isInterface = false;
    private final List<ClassReport> childInnerClasses = new LinkedList<>();
    private final List<MethodReport> methods = new LinkedList<>();
    private final List<FieldReport> fields = new LinkedList<>();

    @Override
    public boolean isInterface() { return this.isInterface; }
    @Override
    public List<ClassReport> getChildInnerClasses() { return this.childInnerClasses; }
    @Override
    public List<MethodReport> getMethods() { return this.methods; }
    @Override
    public List<FieldReport> getFields() { return this.fields; }

    @Override
    public BasicClassReport setIsInterface(boolean isInterface) { this.isInterface = isInterface; return this; }
    @Override
    public BasicClassReport addInnerClasses(Collection<ClassReport> subclasses) { this.childInnerClasses.addAll(subclasses); return this; }
    @Override
    public BasicClassReport addMethods(Collection<MethodReport> methods) { this.methods.addAll(methods); return this; }
    @Override
    public BasicClassReport addFields(Collection<FieldReport> fields) { this.fields.addAll(fields); return this; }

    @Override
    public String toString() {
        String classContent = (
                this.getFields().stream().map(Object::toString).reduce((s1, s2) -> s1 + "\n" + s2).map(s -> "\n" + s).orElse("") +
                this.getMethods().stream().map(Object::toString).reduce((s1, s2) -> s1 + "\n" + s2).map(s -> "\n" + s).orElse("") +
                this.getChildInnerClasses().stream().map(Object::toString).reduce((s1, s2) -> s1 + "\n" + s2).map(s -> "\n" + s).orElse("")
        ).replace("\n", "\n\t");

        return (this.isInterface ? "Interface" : "Class") + "[" +
            this.getAccessSpecifier().asString() + (this.getAccessSpecifier() == AccessSpecifier.PACKAGE_PRIVATE ? "" : " ") +
            this.getName() + " {" +
            classContent + (classContent.isEmpty() ? " " : "\n") +
        "}]";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BasicClassReport that = (BasicClassReport) o;
        return isInterface == that.isInterface && Objects.equals(childInnerClasses, that.childInnerClasses) && Objects.equals(methods, that.methods) && Objects.equals(fields, that.fields);
    }
    @Override
    public int hashCode() { return Objects.hash(super.hashCode(), isInterface, childInnerClasses, methods, fields); }

    /* ### CASTING SUPER METHODS ######################################################################################################################### */
    @Override public BasicClassReport setName(String name) { return (BasicClassReport) super.setName(name); }
    @Override public BasicClassReport setFullName(String fullName) { return (BasicClassReport) super.setFullName(fullName); }
    @Override public BasicClassReport setAccessSpecifier(AccessSpecifier accessSpecifier) { return (BasicClassReport) super.setAccessSpecifier(accessSpecifier); }
    @Override public BasicClassReport addInnerClasses(ClassReport... subclasses) { return (BasicClassReport) ClassReport.super.addInnerClasses(subclasses); }
    @Override public BasicClassReport addMethods(MethodReport... methods) { return (BasicClassReport) ClassReport.super.addMethods(methods); }
    @Override public BasicClassReport addFields(FieldReport... fields) { return (BasicClassReport) ClassReport.super.addFields(fields); }
    @Override public BasicClassReport showContent() { return (BasicClassReport) ClassReport.super.showContent(); }

}
