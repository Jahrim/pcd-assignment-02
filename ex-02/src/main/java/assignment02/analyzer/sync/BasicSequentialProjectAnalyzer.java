package assignment02.analyzer.sync;

import assignment02.PathManager;
import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.packagereport.PackageReport;
import assignment02.mvc.model.packagereport.PartialPackageReport;
import assignment02.mvc.model.packagereport.filereport.FileReport;

import javax.annotation.Nullable;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/** Basic implementation of a sequential project analyzer. */
public class BasicSequentialProjectAnalyzer implements SequentialProjectAnalyzer {

    @Override
    public Optional<ClassReport> getInterfaceReport(File file, String name) throws FileNotFoundException {
        return FileReport.fromFile(file).searchClass(name).filter(ClassReport::isInterface);
    }

    @Override
    public Optional<ClassReport> getClassReport(File file, String name) throws FileNotFoundException {
        return FileReport.fromFile(file).searchClass(name).filter(classReport -> !classReport.isInterface());
    }

    @Override
    public Optional<PackageReport> getPackageReport(File directory) {
        return PathManager.getManager(directory.toPath()).childFilesOf().isEmpty()
               ? Optional.empty()
               : Optional.of(this.extractPackageReport(directory, null));
    }

    @Override
    public Optional<PackageReport> getProjectReport(File directory) {
        return this.getPackageReport(directory).map(projectReport ->
                projectReport
                    .setIsProject(true)
                    .setName(directory.getName())
                    .setFullName(directory.getAbsolutePath())
        );
    }

    /**
     * Support method for {@link #getPackageReport(File)} that keeps track of
     * the parent package in order to build the full name of the current package.
     */
    private PackageReport extractPackageReport(File directory, @Nullable final String parentPackageFullName) {
        List<File> childFiles = PathManager.getManager(directory.toPath()).childFilesOf();
        String packageFullName = parentPackageFullName == null ? "" : (parentPackageFullName + ".") + directory.getName();

        return childFiles.stream()
                         .filter(File::isFile)
                         .map(file -> {
                             try { return FileReport.fromFile(file); }
                             catch (FileNotFoundException e) { e.printStackTrace(); return new PartialPackageReport(); }
                         })
                         .reduce(PartialPackageReport::merge)
                         .orElse(new PartialPackageReport())
                         .seal()
                         .setName(directory.getName())
                         .setFullName(packageFullName)
                         .addSubpackages(
                             childFiles.stream()
                                       .filter(File::isDirectory)
                                       .map(dir -> this.extractPackageReport(dir, packageFullName))
                                       .collect(Collectors.toList())
                         );
    }

}
