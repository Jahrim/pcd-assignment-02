package assignment02.analyzer.async.verticles.handlers;

import assignment02.analyzer.async.verticles.ProjectAnalyzerVerticle;
import assignment02.analyzer.async.verticles.messages.FileMessage;
import assignment02.analyzer.async.verticles.AbstractVerticleHandler;
import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.packagereport.filereport.FileReport;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

import java.io.FileNotFoundException;

/**
 * Models a handler for a {@link FileMessage} responding with the requested
 * file report.
 */
public class FileReportHandler extends AbstractVerticleHandler<ProjectAnalyzerVerticle, JsonObject> {
    
    public FileReportHandler(final ProjectAnalyzerVerticle ownerVerticle) { super(ownerVerticle); }

    @Override
    public void handle(Message<JsonObject> message) {
        FileMessage data = message.body().mapTo(FileMessage.class);
        try {
            FileReport fileReport = FileReport.fromFile(data.file);
            fileReport.getChildClasses().forEach(this::discoverElementsOnClass);
            message.reply(JsonObject.mapFrom(fileReport));
        } catch (FileNotFoundException err) {
            err.printStackTrace();
            message.fail(404, err.toString());
        }
    }

    /**
     * Discovers all elements of the specified class.
     * @param classReport the report of the specified class
     */
    private void discoverElementsOnClass(final ClassReport classReport){
        classReport.getChildInnerClasses().forEach(this::discoverElementsOnClass);
        classReport.getFields().forEach(fieldReport -> this.getOwnerVerticle().discover(fieldReport));
        classReport.getMethods().forEach(methodReport -> this.getOwnerVerticle().discover(methodReport));
        this.getOwnerVerticle().discover(classReport);
    }

}
