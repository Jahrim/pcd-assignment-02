package assignment02.analyzer.async.verticles.messages;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.File;

/** Models a message for getting a class or interface through the event bus of a vertx. */
public class ClassMessage {
    public final File file;
    public final String className;

    public ClassMessage(
        @JsonProperty("file") final File file,
        @JsonProperty("className") final String className
    ){
        this.file = file;
        this.className = className;
    }

    @Override
    public String toString() {
        return "ClassMessage{" +
                "file=" + file +
                ", className='" + className + '\'' +
                '}';
    }
}
