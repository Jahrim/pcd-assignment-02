package assignment02.analyzer.async.verticles;

import assignment02.analyzer.async.verticles.messages.PackageMessage;
import assignment02.analyzer.async.verticles.handlers.ClassReportHandler;
import assignment02.analyzer.async.verticles.handlers.FileReportHandler;
import assignment02.analyzer.async.verticles.handlers.PackageReportHandler;
import assignment02.analyzer.async.verticles.messages.ClassMessage;
import assignment02.analyzer.async.verticles.messages.FileMessage;
import assignment02.mvc.model.ProjectElement;
import assignment02.mvc.model.classreport.ClassReport;
import assignment02.mvc.model.packagereport.PackageReport;
import assignment02.mvc.model.packagereport.filereport.FileReport;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;

import javax.annotation.Nullable;
import java.io.File;
import java.util.Collection;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/** Models a verticle to handle the extraction of project elements from a java project. */
public class ProjectAnalyzerVerticle extends AbstractVerticle {

    private final boolean unique;
    private final Consumer<ProjectElement> onElementDiscovered;

    /** Equivalent of {@link #ProjectAnalyzerVerticle(boolean) ProjectAnalyzerVerticle(false)}. */
    public ProjectAnalyzerVerticle(){ this(false); }
    /** Equivalent of {@link #ProjectAnalyzerVerticle(Consumer, boolean) ProjectAnalyzerVerticle((ignored) -> {}, unique)}. */
    public ProjectAnalyzerVerticle(final boolean unique){ this((ignored) -> {}, unique); }
    /** Equivalent of {@link #ProjectAnalyzerVerticle(Consumer, boolean) ProjectAnalyzerVerticle(onElementDiscovered, true)}. */
    public ProjectAnalyzerVerticle(final Consumer<ProjectElement> onElementDiscovered){ this(onElementDiscovered, true); }
    /**
     * @param onElementDiscovered a callback executed each time a new project element is discovered.
     * @param unique true if the events registered by this specific verticle instance should be
     *                distinguished by the events registered by other instances of this verticle,
     *                false otherwise.
     */
    protected ProjectAnalyzerVerticle(final Consumer<ProjectElement> onElementDiscovered, final boolean unique){
        this.onElementDiscovered = onElementDiscovered;
        this.unique = unique;
    }

    @Override
    public void start(Promise<Void> startPromise) throws Exception {
        super.start();
        this.getVertx().eventBus().consumer(event("getInterfaceReport"), new ClassReportHandler(this, true));
        this.getVertx().eventBus().consumer(event("getClassReport"), new ClassReportHandler(this, false));
        this.getVertx().eventBus().consumer(event("getPackageReport"), new PackageReportHandler(this));
        this.getVertx().eventBus().consumer(event("getFileReport"), new FileReportHandler(this));
        startPromise.complete();
    }

    /**
     * @param file the specified file
     * @param name the name of the specified interface
     * @return a future containing the class report for the specified interface inside the specified file.
     */
    public Future<ClassReport> getInterfaceReport(final File file, final String name) {
        return this.getVertx().eventBus()
                              .request(event("getInterfaceReport"), JsonObject.mapFrom(new ClassMessage(file, name)))
                              .map(response -> ((JsonObject) response.body()).mapTo(ClassReport.class));
    }
    /**
     * @param file the specified file
     * @param name the name of the specified class
     * @return a future containing the class report for the specified class inside the specified file.
     */
    public Future<ClassReport> getClassReport(final File file, final String name) {
        return this.getVertx().eventBus()
                .request(event("getClassReport"), JsonObject.mapFrom(new ClassMessage(file, name)))
                .map(response -> ((JsonObject) response.body()).mapTo(ClassReport.class));
    }

    /**
     * @param files the specified files
     * @return a composite future containing all the file reports of the specified files.
     */
    public CompositeFuture getAllFileReports(final Collection<File> files){
        return CompositeFuture.all(
                files.stream()
                     .filter(File::isFile)
                     .map(this::getFileReport)
                     .collect(Collectors.toList())
        );
    }
    /**
     * @param file the specified file
     * @return a future containing the file report of the specified file.
     */
    public Future<FileReport> getFileReport(final File file){
        return this.getVertx().eventBus()
                              .request(event("getFileReport"), JsonObject.mapFrom(new FileMessage(file)))
                              .map(response -> ((JsonObject) response.body()).mapTo(FileReport.class));
    }

    /**
     * @param directories the specified directories
     * @param parentPackageFullName the name of the parent package, null if these packages have no parent
     * @return a composite future containing all the package reports of the specified directories,
     *         considering the specified name as the name of the parent package.
     */
    public CompositeFuture getAllPackageReports(final Collection<File> directories, @Nullable final String parentPackageFullName){
        return CompositeFuture.all(
                directories.stream()
                           .filter(File::isDirectory)
                           .map((directory) -> getPackageReport(directory, parentPackageFullName, false))
                           .collect(Collectors.toList())
        );
    }
    /**
     * @param directory the specified directory
     * @param parentPackageFullName the name of the parent package, null if this package has no parent
     * @param isProject true if this package report is a project report, false otherwise
     * @return a future containing the package report of the specified directory, considering the
     *         specified name as the name of the parent package.
     */
    public Future<PackageReport> getPackageReport(final File directory, @Nullable final String parentPackageFullName, boolean isProject){
        return this.getVertx().eventBus()
                              .request(event("getPackageReport"), JsonObject.mapFrom(new PackageMessage(directory, parentPackageFullName, isProject)))
                              .map(response -> ((JsonObject) response.body()).mapTo(PackageReport.class));
    }

    /**
     * Discovers the specified project element calling the adequate callback.
     * @param element the specified project element
     */
    public void discover(final ProjectElement element){ this.onElementDiscovered.accept(element); }

    /**
     * @param name the name of the specified event
     * @return a new name for the specified event dependent on this verticle context.
     */
    private String event(final String name){ return name + ((unique) ? this.deploymentID() : ""); }
}
