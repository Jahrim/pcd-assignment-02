package assignment02.analyzer.async.verticles;

import io.vertx.core.Verticle;
import io.vertx.core.eventbus.Message;

/** Models an abstract verticle handler. See {@link io.vertx.core.Handler}. */
public abstract class AbstractVerticleHandler<V extends Verticle, M> implements io.vertx.core.Handler<Message<M>> {

    private final V ownerVerticle;

    protected AbstractVerticleHandler(final V ownerVerticle){ this.ownerVerticle = ownerVerticle; }

    /** @return the verticle that owns this handler. */
    protected V getOwnerVerticle() { return this.ownerVerticle; }

}
