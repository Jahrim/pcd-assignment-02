package assignment02.main.assignment01;

import assignment02.concurrency.masterworker.master.SinglePoolMaster;
import assignment02.mvc.controller.Controller;
import assignment02.mvc.controller.simulator.concurrent.ConcurrentSimulator;
import assignment02.mvc.view.SimulationView;

/**
 * Concurrent simulation with view.
 */
public class MainWithGUI {

    private static final int MAX_WORKERS = Runtime.getRuntime().availableProcessors() + 1;

    private static final int nBodies = 1000;
    private static final int nIterations = 5000;
    private static final int nWorkers = MAX_WORKERS;

    public static void main(String[] args) {
        Controller controller = new Controller(new ConcurrentSimulator(new SinglePoolMaster(nWorkers)));
    	SimulationView viewer = new SimulationView(620,620);
        long time = controller.attachView(viewer).runSimulation(nBodies, nIterations);

        final SimulationArgs simArgs = new SimulationArgs(nBodies, nIterations, nWorkers);
        System.out.println(simArgs + " => " + String.format("%.2f", time/1000f) + "s");
    }
}
