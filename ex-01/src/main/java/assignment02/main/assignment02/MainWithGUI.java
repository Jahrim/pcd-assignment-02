package assignment02.main.assignment02;

import assignment02.concurrency.masterworker.master.ExecutorMaster;
import assignment02.mvc.controller.Controller;
import assignment02.mvc.controller.simulator.concurrent.ConcurrentSimulator;
import assignment02.mvc.view.SimulationView;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Concurrent simulation with view.
 */
public class MainWithGUI {

    private static final int MAX_WORKERS = Runtime.getRuntime().availableProcessors() + 1;

    private static final int nBodies = 1000;
    private static final int nIterations = 5000;
    private static final String executorName = "CachedThreadPool";
    private static final ExecutorService executor = Executors.newCachedThreadPool();

    public static void main(String[] args) {
        Controller controller = new Controller(new ConcurrentSimulator(new ExecutorMaster(executor)));
    	SimulationView viewer = new SimulationView(620,620);
        long time = controller.attachView(viewer).runSimulation(nBodies, nIterations);

        final SimulationArgs simArgs = new SimulationArgs(nBodies, nIterations, executorName, executor);
        System.out.println(simArgs + " => " + String.format("%.2f", time/1000f) + "s");
    }
}
