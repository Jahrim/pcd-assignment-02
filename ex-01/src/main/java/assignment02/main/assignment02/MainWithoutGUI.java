package assignment02.main.assignment02;

import assignment02.concurrency.masterworker.master.ExecutorMaster;
import assignment02.mvc.controller.Controller;
import assignment02.mvc.controller.simulator.concurrent.ConcurrentSimulator;
import assignment02.util.data.Pair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

/**
 * Executes a concurrent simulation without view for all the possible combinations
 * of the specified arguments.
 */
public class MainWithoutGUI {

    private static final int MAX_WORKERS = Runtime.getRuntime().availableProcessors() + 1;

    /** The set of #bodies arguments to test. */
    private static List<Integer> nBodiesArgs = List.of(100, 1000, 5000);
    /** The set of #iterations arguments to test. */
    private static List<Integer> nIterationsArgs = List.of(1000, 5000, 10000);
    /** The set of #workers arguments to test. */
    private static List<Pair<String, Supplier<ExecutorService>>> executorArgs = List.of(
        Pair.of("FixedThreadPool_1", () -> Executors.newFixedThreadPool(1)),
        Pair.of("FixedThreadPool_4", () -> Executors.newFixedThreadPool(4)),
        Pair.of("FixedThreadPool_8", () -> Executors.newFixedThreadPool(8)),
        Pair.of("FixedThreadPool_N", () -> Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors() + 1)),
        Pair.of("CachedThreadPool", Executors::newCachedThreadPool)
    );
    /** A map from a specified tuple of arguments to the time of execution of the correspondent simulation. */
    private static Map<SimulationArgs, Long> timeMap = new HashMap<>();

    public static void main(String[] args) {
        for (Integer nBodies: nBodiesArgs) {
            for (Integer nIterations: nIterationsArgs) {
                for (Pair<String, Supplier<ExecutorService>> executor: executorArgs) {
                    SimulationArgs simArgs = new SimulationArgs(nBodies, nIterations, executor._1, executor._2.get());
                    System.out.println("Simulating " + simArgs + "...");
                    long time = MainWithoutGUI.simulateConcurrent(simArgs);
                    timeMap.put(simArgs, time);
                }
            }
        }
        timeMap.forEach( (simArgs, time) -> {
            System.out.println(simArgs.toString() + " => " + String.format("%.2f", time/1000f) + "s");
        });
        System.exit(0);
    }

    /**
     * Executes a concurrent simulation with the specified arguments.
     * @param simArgs the arguments for the concurrent simulation
     * @return the time of execution for the specified simulation.
     */
    public static long simulateConcurrent(SimulationArgs simArgs){
        return new Controller(
                new ConcurrentSimulator(
                        new ExecutorMaster(simArgs.executor)
                )
        ).runSimulation(simArgs.nBodies, simArgs.nIterations);
    }
}
