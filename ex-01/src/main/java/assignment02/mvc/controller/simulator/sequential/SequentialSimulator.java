package assignment02.mvc.controller.simulator.sequential;

import assignment02.mvc.controller.simulator.AbstractSimulator;
import assignment02.mvc.model.Body;
import assignment02.util.math.V2d;

/**
 * Models a simulator whose execution is sequential.
 */
public class SequentialSimulator extends AbstractSimulator {

	@Override
	protected void onSimulationSetup() { }

	@Override
	protected void onSimulationStep() {
		for (Body b: this.bodies) {														//compute forces & update accelerations
			V2d totalForce = b.computeTotalForceOnSelf(this.bodies);
			b.updateAcceleration(totalForce);
		}
		for (Body b : this.bodies) { b.updateVelocity(this.dt); }						//update velocities
		for (Body b : this.bodies) { b.updatePosition(this.dt); }						//update positions
		for (Body b : this.bodies) { b.checkAndSolveBoundaryCollision(this.bounds); }	//check collisions with boundary
	}

	@Override
	protected void onSimulationEnd() { }

}
