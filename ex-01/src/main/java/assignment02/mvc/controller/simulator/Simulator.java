package assignment02.mvc.controller.simulator;

/**
 * Models a simulator for a simulation consisting in an iteration of different steps.
 */
public interface Simulator {

    /**
     * Executes the specified number of iterations for this simulation.
     * @param maxIterations the specified number of iterations
     */
    void loop(final long maxIterations);

}
