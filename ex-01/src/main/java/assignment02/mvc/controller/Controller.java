package assignment02.mvc.controller;

import assignment02.mvc.controller.simulator.AbstractSimulator;
import assignment02.util.time.StopWatch;
import assignment02.mvc.view.SimulationView;

/**
 * Models the controller for this simulation.
 */
public class Controller {

    private final AbstractSimulator simulator;

    public Controller(AbstractSimulator simulator){ this.simulator = simulator; }

    /**
     * Executes a simulation with the specified number of bodies for the specified number
     * of iterations.
     * @param nBodies the specified number of bodies
     * @param nIterations the specified number of iterations
     * @return the time of execution for the simulation.
     */
    public long runSimulation(int nBodies, int nIterations){
        StopWatch timer = new StopWatch().next();
        this.simulator.testCustomBodySet(nBodies).loop(nIterations);
        return timer.getDuration();
    }

    /**
     * Attach the specified viewer to this controller and vice versa.
     * @param viewer the specified viewer
     * @return this
     */
    public Controller attachView(SimulationView viewer){
        this.simulator.attachView(viewer);
        viewer.attachController(this);
        return this;
    }

    /**
     * Notifies the controller that the simulation should be started.
     * @return this
     */
    public Controller notifyStartSimulation() { this.simulator.startSimulation(); return this; }
    /**
     * Notifies the controller that the simulation should be stopped.
     * @return this
     */
    public Controller notifyStopSimulation() { this.simulator.stopSimulation(); return this; }
}
