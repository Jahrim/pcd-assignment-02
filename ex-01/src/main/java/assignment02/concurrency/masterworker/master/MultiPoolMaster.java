package assignment02.concurrency.masterworker.master;

import assignment02.concurrency.masterworker.taskpool.Task;
import assignment02.concurrency.masterworker.taskpool.TaskPool;
import assignment02.concurrency.masterworker.worker.Worker;
import assignment02.util.exception.UsedWhenClosedException;

import java.util.ArrayList;
import java.util.List;

/**
 * Models a master whose job is to distribute tasks among a set of workers.
 * The workers used by this master use different task pools.
 */
public class MultiPoolMaster extends AbstractMaster {
    private final List<TaskPool> taskPools;

    public MultiPoolMaster(int nWorkers) { this(nWorkers, 50); }
    public MultiPoolMaster(int nWorkers, int taskBatchSize) {
        super();
        this.taskPools = new ArrayList<>();
        for (int i = 0; i < nWorkers; i++){
            TaskPool taskPool = new TaskPool();
            this.taskPools.add(taskPool);
            this.workers.add(new Worker(taskPool, taskBatchSize));
        }
    }

    @Override
    protected final MultiPoolMaster distributeTasks(final List<Task> tasks) {
        List<List<Task>> tasksPerPool = new ArrayList<>();
        this.taskPools.forEach( (taskPool) -> tasksPerPool.add(new ArrayList<>()) );
        for (int i = 0, j = 0; i < tasks.size(); i++, j = (j + 1) % this.taskPools.size()){
            tasksPerPool.get(j).add(tasks.get(i));
        }
        for (int i = 0; i < tasksPerPool.size(); i++){
            try { this.taskPools.get(i).addTasks(tasksPerPool.get(i)); } catch (UsedWhenClosedException e) { e.printStackTrace(); }
        }
        return this;
    }

    @Override
    public MultiPoolMaster killWorkers() {
        this.taskPools.forEach( (taskPool) -> {
            try { taskPool.close(); } catch (UsedWhenClosedException e) { e.printStackTrace(); }
        });
        return this;
    }
}
