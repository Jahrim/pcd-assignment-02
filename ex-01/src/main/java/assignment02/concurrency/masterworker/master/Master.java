package assignment02.concurrency.masterworker.master;

import assignment02.concurrency.masterworker.taskpool.Task;

import java.util.List;

/**
 * Models a master which handles the distribution of tasks among
 * a set of workers to support concurrent execution for those tasks.
 */
public interface Master {

    /**
     * Concurrently executes the specified tasks, waiting for the computation to be completed.
     * @return this
     * @throws InterruptedException if the wait for this thread is interrupted.
     */
    Master syncExecute(List<Task> tasks) throws InterruptedException;

    /**
     * Concurrently executes the specified tasks.
     * @return this
     */
    Master asyncExecute(List<Task> tasks);

    /**
     * Kills all active workers.
     * @return this
     */
    Master killWorkers();

}
