package assignment02.concurrency.masterworker.taskpool;

import assignment02.util.exception.UsedWhenClosedException;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * [Monitor]
 * Models a task pool containing a set of tasks to be dispatched.
 */
public class TaskPool {

    private final List<Task> tasks;
    private boolean isOpen;

    public TaskPool(){
        this.tasks = new LinkedList<>();
        this.isOpen = true;
    }

    /** @return true if this task pool is open, false otherwise. */
    public synchronized boolean isOpen() {
        return this.isOpen;
    }

    /** Closes this task pool. */
    public synchronized TaskPool close() throws UsedWhenClosedException {
        requireOpen();
        this.isOpen = false;
        this.notifyAll();
        return this;
    }

    /**
     * Waits for this task pool to be depleted.
     * @return this
     * @throws InterruptedException if the wait for this thread is interrupted.
     * @throws UsedWhenClosedException if this task pool was previously closed.
     */
    public synchronized TaskPool awaitDepletion() throws InterruptedException, UsedWhenClosedException {
        requireOpen();
        while(!this.tasks.isEmpty()){ this.wait(); }
        return this;
    }

    /**
     * Waits for a task to be available in this task pool.
     * @return this
     * @throws InterruptedException if the wait for this thread is interrupted.
     * @throws UsedWhenClosedException if this task pool was previously closed.
     */
    public synchronized TaskPool awaitTask() throws InterruptedException, UsedWhenClosedException {
        requireOpen();
        while(this.tasks.isEmpty()){ this.wait(); }
        return this;
    }

    /**
     * Waits for a task to be available in this task pool and pops the specified number of
     * tasks from this pool. If there are not enough tasks in the pool, it will return all
     * the tasks of this pool.
     * @param maxTasks the maximum number of tasks to be extracted
     * @return the first available tasks
     * @throws InterruptedException if the wait for this thread is interrupted.
     * @throws UsedWhenClosedException if this task pool was previously closed.
     */
    public synchronized List<Task> awaitAndPopTasks(final int maxTasks) throws InterruptedException, UsedWhenClosedException {
        return this.awaitTask().popTasks(maxTasks);
    }

    /**
     * Adds the specified tasks to this pool of tasks.
     * @param tasks the specified tasks
     * @return this
     * @throws UsedWhenClosedException if this task pool was previously closed.
     */
    public synchronized TaskPool addTasks(final List<Task> tasks) throws UsedWhenClosedException {
        requireOpen();
        this.tasks.addAll(tasks);
        this.notifyAll();
        return this;
    }

    /**
     * Adds the specified task to this pool of tasks.
     * @param task the specified task
     * @return this
     * @throws UsedWhenClosedException if this task pool was previously closed.
     */
    public synchronized TaskPool addTask(final Task task) throws UsedWhenClosedException {
        return this.addTasks(List.of(task));
    }

    /**
     * @param maxTasks the maximum number of tasks to be extracted
     * @return the specified number of tasks extracted from this pool of tasks. If there are not
     *         enough tasks in the pool, it will return all the tasks of this pool.
     * @throws UsedWhenClosedException if this task pool was previously closed.
     */
    private synchronized List<Task> popTasks(final int maxTasks) throws UsedWhenClosedException {
        requireOpen();
        List<Task> extractedTasks = new ArrayList<>();
        for (int i=0; i<maxTasks && this.tasks.size() > 0; i++){
            extractedTasks.add(this.tasks.remove(0));
        }
        this.notifyAll();
        return extractedTasks;
    }

    /**
     * @return this
     * @throws UsedWhenClosedException if this task pool is closed.
     */
    private synchronized TaskPool requireOpen() throws UsedWhenClosedException {
        if (!this.isOpen){ throw new UsedWhenClosedException(); }
        return this;
    }
}
