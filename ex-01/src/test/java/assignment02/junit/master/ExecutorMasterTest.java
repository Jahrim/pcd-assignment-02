package assignment02.junit.master;

import assignment02.concurrency.masterworker.master.ExecutorMaster;
import java.util.concurrent.Executors;

public class ExecutorMasterTest extends MasterTest {
    protected ExecutorMasterTest() {
        super(new ExecutorMaster(Executors.newCachedThreadPool()));
    }
}
