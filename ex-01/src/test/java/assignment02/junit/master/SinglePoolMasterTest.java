package assignment02.junit.master;

import assignment02.concurrency.masterworker.master.SinglePoolMaster;

public class SinglePoolMasterTest extends MasterTest {
    protected SinglePoolMasterTest() {
        super(new SinglePoolMaster(Runtime.getRuntime().availableProcessors() + 1));
    }
}
