package assignment02.junit.master;

import assignment02.concurrency.masterworker.master.Master;
import assignment02.concurrency.masterworker.taskpool.Task;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public abstract class MasterTest {

    private static final int MAX_ITERATIONS = 10000;
    private static final int UNTIL = 1000;

    protected final Master master;

    protected MasterTest(Master master) {
        this.master = master;
    }

    @Test
    void TestTaskDependencies() {
        final SyncCounter counter1 = new SyncCounter(0);
        final SyncCounter counter2 = new SyncCounter(0);
        final SyncCounter counter3 = new SyncCounter(0);

        final int maxIterations = MasterTest.MAX_ITERATIONS;
        final int until = MasterTest.UNTIL;

        for (int i = 0; i < maxIterations; i++) {
            try {
                this.syncCount(counter1, until);
                    assertEquals(until, counter1.getCount());
                    assertEquals(0, counter2.getCount());
                    assertEquals(0, counter3.getCount());
                this.syncCount(counter2, until);
                    assertEquals(until, counter1.getCount());
                    assertEquals(until, counter2.getCount());
                    assertEquals(0, counter3.getCount());
                this.syncCount(counter3, until);
                    assertEquals(until, counter1.getCount());
                    assertEquals(until, counter2.getCount());
                    assertEquals(until, counter3.getCount());
                counter1.reset(0);
                counter2.reset(0);
                counter3.reset(0);
            } catch (InterruptedException ex) { ex.printStackTrace(); }
        }
    }

    /**
     * Concurrently increments the specified counter for the specified number of times,
     * waiting for the computation to be completed.
     * @param counter the specified counter
     * @param until the specified number of increments
     * @return this
     * @throws InterruptedException if the wait for this thread is interrupted.
     */
    private Master syncCount(final SyncCounter counter, int until) throws InterruptedException {
        List<Task> tasks = new ArrayList<>();
        for(int i = 0; i < until; i++) { tasks.add(counter::increment); }
        return this.master.syncExecute(tasks);
    }

}
